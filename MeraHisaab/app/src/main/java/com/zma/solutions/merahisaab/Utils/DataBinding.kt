package com.zma.solutions.merahisaab.Utils

import com.google.gson.JsonObject
import com.zma.solutions.merahisaab.model.UserInfo

object DataBinding {


    fun addUser(userinfo: UserInfo, pickDate: String): JsonObject {

        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERPHONENUMBER, userinfo.userPhone)
        jsonObject.addProperty(Constant.MESSAGETYPE, userinfo.messageType)
        jsonObject.addProperty(Constant.USERNAME, "")
        jsonObject.addProperty(Constant.USERID, userinfo.userId)
        jsonObject.addProperty(Constant.CREATE_DATE_TIME, pickDate)

        return jsonObject

    }


    fun addBusiness(userinfo: UserInfo?, businessName: String, pickDate: String): JsonObject {

        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.BUSSINESS_NAME, businessName)
        jsonObject.addProperty(Constant.MESSAGETYPE, userinfo!!.messageType)
        jsonObject.addProperty(Constant.CREATE_DATE_TIME, pickDate)
        jsonObject.addProperty(Constant.UPDATE_BUSINESS_DATE, pickDate)
        jsonObject.addProperty(Constant.USERID, userinfo.userId)

        return jsonObject

    }

    fun getAllBusiness(userinfo: UserInfo?): JsonObject {

        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.MESSAGETYPE, userinfo!!.messageType)
        jsonObject.addProperty(Constant.USERID, userinfo!!.userId)

        return jsonObject
    }

    fun getAllCustomers(userinfo: UserInfo?): JsonObject {

        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERID, userinfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userinfo!!.messageType)
        jsonObject.addProperty(Constant.BUSSINESS_ID, userinfo.businessId)


        return jsonObject

    }

    fun addCustomer(
        userInfo: UserInfo?,
        name: String,
        number: String,
        pickDate: String
    ): JsonObject {

        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userInfo!!.messageType)
        jsonObject.addProperty(Constant.BUSSINESS_ID, userInfo.businessId)
        jsonObject.addProperty(Constant.CUSTOMER_NAME, name)
        jsonObject.addProperty(Constant.CUSTOMER_PHONE_NUMBER, number)
        jsonObject.addProperty(Constant.YOU_WILL_GET, "0")
        jsonObject.addProperty(Constant.YOU_WILL_GIVE, "0")
        jsonObject.addProperty(Constant.CUSTOMER_ADDRESS, "")
        jsonObject.addProperty(Constant.CREATE_DATE_TIME, pickDate)

        return jsonObject

    }

    fun getCustomerItemDetail(
        userInfo: UserInfo?,
        customerId: String?,
        businessId: String?
    ): JsonObject {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userInfo!!.messageType)
        jsonObject.addProperty(Constant.BUSSINESS_ID, businessId)
        jsonObject.addProperty(Constant.CUSTOMER_ID, customerId)

        return jsonObject

    }

    fun setCustomerItemDetail(
        userInfo: UserInfo?,
        customerId: String,
        businessId: String,
        date: String,
        amountGet: String,
        amountGive: String,
        itemDetail: String
    ): JsonObject {

        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userInfo!!.messageType)
        jsonObject.addProperty(Constant.BUSSINESS_ID, businessId)
        jsonObject.addProperty(Constant.CUSTOMER_ID, customerId)

        jsonObject.addProperty(Constant.DATE_CREATED, date)
        jsonObject.addProperty(Constant.YOU_GOT, amountGet)
        jsonObject.addProperty(Constant.YOU_GAVE, amountGive)
        jsonObject.addProperty(Constant.DETAIL, itemDetail)

        return jsonObject

    }

    fun setUpdateCustomerItemDetail(
        userInfo: UserInfo?,
        customerId: String,
        businessId: String,
        date: String,
        amountGet: String,
        amountGive: String,
        itemDetail: String,
        recordId: String
    ): JsonObject {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userInfo!!.messageType)
        jsonObject.addProperty(Constant.BUSSINESS_ID, businessId)
        jsonObject.addProperty(Constant.CUSTOMER_ID, customerId)

        jsonObject.addProperty(Constant.DATE_CREATED, date)
        jsonObject.addProperty(Constant.YOU_GOT, amountGet)
        jsonObject.addProperty(Constant.YOU_GAVE, amountGive)
        jsonObject.addProperty(Constant.DETAIL, itemDetail)
        jsonObject.addProperty(Constant.RECORD_ID, recordId)

        return jsonObject

    }

    fun deleteCustomerItemDetail(
        userInfo: UserInfo?,
        customerId: String,
        businessId: String,
        recordId: String
    ): JsonObject {
        var jsonObject = JsonObject()
        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userInfo!!.messageType)

        jsonObject.addProperty(Constant.BUSSINESS_ID, businessId)
        jsonObject.addProperty(Constant.CUSTOMER_ID, customerId)
        jsonObject.addProperty(Constant.RECORD_ID, recordId)

        return jsonObject
    }

    fun getCustomerDetail(
        userInfo: UserInfo?,
        customerId: String?,
        businessId: String?
    ): JsonObject {

        var jsonObject = JsonObject()

        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)
        jsonObject.addProperty(Constant.MESSAGETYPE, userInfo!!.messageType)
        jsonObject.addProperty(Constant.BUSSINESS_ID, businessId)
        jsonObject.addProperty(Constant.CUSTOMER_ID, customerId)

        return jsonObject
    }

    fun updateCustomer(id: String?, name: String, number: String, date: String): JsonObject {
        var jsonObject = JsonObject()

        jsonObject.addProperty(Constant.CUSTOMER_PHONE_NUMBER, number)
        jsonObject.addProperty(Constant.MESSAGETYPE, "1")
        jsonObject.addProperty(Constant.CUSTOMER_NAME, name)
        jsonObject.addProperty(Constant.CUSTOMER_ID, id)
        jsonObject.addProperty(Constant.DATE_CREATED, date)
        jsonObject.addProperty(Constant.CUSTOMER_ADDRESS, "")

        return jsonObject
    }

    fun updateBusiness(bId: String, businessName: String, pickDate: String): JsonObject {
        var jsonObject = JsonObject()

        jsonObject.addProperty(Constant.BUSSINESS_NAME, businessName)
        jsonObject.addProperty(Constant.MESSAGETYPE, "1")
        jsonObject.addProperty(Constant.BUSSINESS_ID, bId)
        jsonObject.addProperty(Constant.DATE_CREATED, pickDate)
        return jsonObject

    }

    fun deleteBusiness(id: String?): JsonObject {
        var jsonObject = JsonObject()

        jsonObject.addProperty(Constant.MESSAGETYPE, "1")
        jsonObject.addProperty(Constant.BUSSINESS_ID, id)

        return jsonObject

    }

    fun getAllBusinessCustomer(userInfo: UserInfo): JsonObject {
        var jsonObject = JsonObject()

        jsonObject.addProperty(Constant.MESSAGETYPE, "1")
        jsonObject.addProperty(Constant.BUSSINESS_ID, userInfo.businessId)
        jsonObject.addProperty(Constant.USERID, userInfo!!.userId)

        return jsonObject
    }
}