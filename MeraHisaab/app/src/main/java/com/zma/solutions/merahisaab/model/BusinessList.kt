package com.zma.solutions.merahisaab.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class BusinessList {

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("businessList")
    @Expose
    var businessList: List<BusinessItem>? = null

    inner class BusinessItem {

        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("DateCreated")
        @Expose
        var date: String? = null

        @SerializedName("BusinessName")
        @Expose
        var businessName: String? = null

        @SerializedName("UserId")
        @Expose
        var userId: String? = null

        @SerializedName("TotalCustomers")
        @Expose
        var TotalCustomers: String? = null


    }
}