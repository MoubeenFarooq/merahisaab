package com.zma.solutions.merahisaab.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.adapter.MyCustomerListItemDetailAdapter
import com.zma.solutions.merahisaab.listeners.CustomerItemDetailClickListener
import com.zma.solutions.merahisaab.model.CustomerItemDetail
import com.zma.solutions.merahisaab.model.CustomerList
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.androidexception.andexalertdialog.AndExAlertDialog
import kotlinx.android.synthetic.main.activity_item_detail.*


class CustomerItemDetailActivity : AppCompatActivity(), CustomerItemDetailClickListener,
    PopupMenu.OnMenuItemClickListener {


    lateinit var view: View
    var userInfo: UserInfo? = null
    var context: CustomerItemDetailActivity = this
    var customerItem: CustomerList.CustomerListItem? = null
    var customeMainItem: CustomerItemDetail? = null
    var pDialog: SweetAlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_detail)
        userInfo = PrefManger.getUser(context)

        pDialog = Util.init_Progress(context)

        setFontAwsom()

        getDataFromPreviousActivity()

        setClickListeners()
    }

    private fun setFontAwsom() {
        Util.FONT_AWSOME(context, ImBack)
        Util.FONT_AWSOME(context, imOption)
        // Util.FONT_AWSOME(context, tvWhatsapp)
        Util.FONT_AWSOME(context, tvMessage)
    }

    private fun setClickListeners() {


        llYouGave.setOnClickListener {
            gotoNextActivityAdd("give")
        }

        llYouGot.setOnClickListener {
            gotoNextActivityAdd("get")
        }

        ImBack.setOnClickListener {
            onBackPressed()
        }

        tvMessage.setOnClickListener {
            gotoShareActivity()
        }
    }


    fun showPopup(v: View) {
        val popup = PopupMenu(this, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.menu_main, popup.menu)
        popup.setOnMenuItemClickListener(this)
        popup.gravity = Gravity.END
        popup.show()
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_update -> {
                updateCustomer()
                return true
            }
            R.id.action_delete -> {
                showDialog("Alert", "Do you want to delete customer.", context)
                return true
            }
            R.id.action_report -> {
                generateSingleReport()
                return true
            }
        }
        return false
    }

    private fun generateSingleReport() {
        val intent = Intent(this, GenerateSingleReport::class.java)
        val bundle = Bundle()
        bundle.putParcelable("customerItem", customeMainItem)
        intent.putExtra("customerName", customerItem!!.customerName)
        intent.putExtra("Bundle", bundle)
        startActivity(intent)
    }

    private fun updateCustomer() {
        val intent = Intent(this, AddCustomerActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable("customerItem", customerItem)
        intent.putExtra("action", "update")
        intent.putExtra("Bundle", bundle)
        startActivity(intent)
        finish()
    }


    private fun gotoShareActivity() {
        val intent = Intent(this, CustomerItemShare::class.java)
        val bundle = Bundle()

        bundle.putParcelable("customerItem", customerItem)
        intent.putExtra("Bundle", bundle)
        startActivity(intent)

    }


    private fun gotoNextActivityAdd(action: String) {
        val intent = Intent(this, CustomerItemAddUpdateRemove::class.java)
        val bundle = Bundle()

        bundle.putParcelable("customerItem", customerItem)
        intent.putExtra("Bundle", bundle)
        intent.putExtra("action", action)
        intent.putExtra("function", "add")
        startActivity(intent)
    }

    private fun getDataFromPreviousActivity() {

        val bundle = intent.getBundleExtra("Bundle")
        customerItem =
            bundle.getParcelable<CustomerList.CustomerListItem>("customerItem")//its mean not null
        settingRecyclerView(customerItem)

    }

    private fun settingRecyclerView(customerItem: CustomerList.CustomerListItem?) {

        val layoutManger = LinearLayoutManager(this)
        layoutManger.orientation = LinearLayoutManager.VERTICAL
        listCustomerItemDetail.layoutManager = layoutManger
        tvCustomerName.text = customerItem!!.customerName
        setMainAmount(customerItem.youWillGet, customerItem.youWillGive)
        pDialog!!.show()
        
        if (Util.DETECT_INTERNET_CONNECTION(context)) {
            getCustomerItemDetailList(customerItem)
        } else {
            Util.showDialog("Alert", "No Internet Connection", context)
        }
    }

    private fun setMainAmount(youWillGet: String?, youWillGive: String?) {

        when {
            youWillGive != "0" -> {
                tvTotalAmount.text = "RS :$youWillGive"
                tvTotalAmount.setTextColor(resources.getColor(R.color.red))
                tvTitle.text = "You Will Give"
                tvTitle.setTextColor(resources.getColor(R.color.red))
            }
            youWillGet != "0" -> {
                tvTotalAmount.text = "RS :$youWillGet"
                tvTitle.text = "You Will Get"
                tvTotalAmount.setTextColor(resources.getColor(R.color.color_3_dark))
                tvTitle.setTextColor(resources.getColor(R.color.color_3_dark))
            }
            else -> {
                tvTotalAmount.text = "RS :$youWillGet"
                tvTitle.text = "Settled Up"
                tvTotalAmount.setTextColor(resources.getColor(R.color.diGREY0))
            }
        }
    }


    private fun getCustomerItemDetailList(customerId: CustomerList.CustomerListItem?) {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.getCustomerItemDetailFromServer(
                DataBinding.getCustomerItemDetail(
                    userInfo,
                    customerId!!.id,
                    customerId.businessId
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        if (it.customerDetailList!!.size > 0) {
                            customeMainItem = it
                            setMainAmount(it.youWillGet, it.youWillGive)
                            setAdapterOfCustomerDetail(it.customerDetailList!!)
                            if (customerItem!!.customerPhoneNumber == "") {
                                //  tvWhatsapp.visibility = View.GONE
                                tvMessage.visibility = View.GONE
                            }
                        } else {
                            // tvWhatsapp.visibility = View.GONE
                            tvMessage.visibility = View.GONE
                        }
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }


    private fun deleteCustomer() {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.deleteCustomerFromServer(
                DataBinding.getCustomerDetail(
                    userInfo,
                    customerItem!!.id,
                    customerItem!!.businessId
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        finish()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )

    }

    private fun setAdapterOfCustomerDetail(customerDetailList: List<CustomerItemDetail.CustomerDetailListItem>) {

        val adapter =
            MyCustomerListItemDetailAdapter(context, customerDetailList, this)
        listCustomerItemDetail.adapter = adapter
    }


    override fun customerDetailItem(customerDetailItem: CustomerItemDetail.CustomerDetailListItem) {
        gotoNextActivityUpdate(customerDetailItem)
    }


    private fun gotoNextActivityUpdate(
        customerItemDetail: CustomerItemDetail.CustomerDetailListItem
    ) {

        val intent = Intent(this, CustomerItemDetailShare::class.java)
        val bundle = Bundle()

        if (customerItemDetail.youGave.equals("0")) {
            intent.putExtra("action", "get")
        } else {
            intent.putExtra("action", "give")
        }

        bundle.putParcelable("customerItemDetail", customerItemDetail)
        bundle.putParcelable("customerItem", customerItem)

        intent.putExtra("Bundle", bundle)
        intent.putExtra("function", "update")
        startActivity(intent)
    }


    override fun onResume() {
        super.onResume()

        if (Util.DETECT_INTERNET_CONNECTION(context)) {
            pDialog!!.show()
            getCustomerItemDetailList(customerItem)
        } else {
            Util.showDialog("Alert", "No Internet Connection", context)
        }
    }

    fun showDialog(title: String, message: String, contaxt: Context) {


        AndExAlertDialog.Builder(contaxt)
            .setTitle(title)
            .setMessage(message)
            .setPositiveBtnText("ok")
            .setNegativeBtnText("cancel")
            .setCancelableOnTouchOutside(false)
            .OnPositiveClicked {

                if (Util.DETECT_INTERNET_CONNECTION(context)) {
                    pDialog!!.show()
                    deleteCustomer()
                } else {
                    Util.showDialog("Alert", "No Internet Connection", context)
                }
            }
            .OnNegativeClicked {

            }
            .build()

    }


}








