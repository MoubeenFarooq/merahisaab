package com.zma.solutions.merahisaab.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.model.CustomerItemDetail
import com.zma.solutions.merahisaab.model.CustomerList
import com.zma.solutions.merahisaab.model.UserInfo
import kotlinx.android.synthetic.main.activity_adding_update_item.ImBack
import kotlinx.android.synthetic.main.activity_adding_update_item.tvDate
import kotlinx.android.synthetic.main.activity_detail_share.*


class CustomerItemDetailShare : AppCompatActivity() {

    lateinit var view: View
    val context: CustomerItemDetailShare = this
    var customerItem: CustomerList.CustomerListItem? = null
    var customerItemUpdate: CustomerItemDetail.CustomerDetailListItem? = null

    var userInfo: UserInfo? = null
    var action = ""
    var businessId = ""
    var function = ""
    var pDialog: SweetAlertDialog? = null

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_share)

        userInfo = PrefManger.getUser(context)

        pDialog = Util.init_Progress(context)

        setFontAwsome()
        getDataFromPreviousActivity()

        clickListeners()
    }

    private fun setFontAwsome() {
        Util.FONT_AWSOME(context, ImBack)
        Util.FONT_AWSOME(context, tvEdit)
        Util.FONT_AWSOME(context, tvWhatsapp)
        Util.FONT_AWSOME(context, tvMessage)
    }


    private fun getDataFromPreviousActivity() {

        action = intent.getStringExtra("action")
        function = intent.getStringExtra("function")

        val bundle = intent.getBundleExtra("Bundle")


        customerItem = bundle.getParcelable("customerItem")//its mean not null
        customerItemUpdate = bundle.getParcelable("customerItemDetail")//its mean not null


        if (action == "get") {
            tvCustomerAction.text = "Gave"
            tvAmount.text = "Rs. " + customerItemUpdate!!.youGot
            tvAmount.setTextColor(resources.getColor(R.color.diGreen))

        } else {
            tvCustomerAction.text = "took"
            tvAmount.text = "Rs. " + customerItemUpdate!!.youGave
            tvAmount.setTextColor(resources.getColor(R.color.diOrange))
        }

        tvCustomerName.text = customerItem!!.customerName
        tvDate.text = "On " + customerItemUpdate!!.dateCreated
        tvDateFinal.text = Util.pick_date()

        when {
            customerItem!!.youWillGive != "0" -> {
                tvTotalAmount.text = "Balance Rs:${customerItem!!.youWillGive}"
            }
            customerItem!!.youWillGet != "0" -> {
                tvTotalAmount.text = "Balance Rs:${customerItem!!.youWillGet}"
            }
        }

        if (customerItemUpdate!!.details!!.isNotEmpty()) {
            tvNote.visibility = View.VISIBLE
            tvNoteDetail.visibility = View.VISIBLE
            tvNoteDetail.text = customerItemUpdate!!.details
        }
    }


    private fun clickListeners() {
        tvWhatsapp.setOnClickListener {
            var msg = setMsg()

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.setPackage("com.whatsapp")
            intent.putExtra(Intent.EXTRA_TEXT, msg)
            if (intent.resolveActivity(packageManager) == null) {
                Util.showToast("Please Install WhatsApp", it)
            } else {
                startActivity(intent)
            }
        }

        tvMessage.setOnClickListener {
            var msg = setMsg()
            val smsIntent = Intent(Intent.ACTION_VIEW)

            smsIntent.data = Uri.parse("smsto:")
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("address", customerItem!!.customerPhoneNumber)
            smsIntent.putExtra("sms_body", msg)

            try {
                startActivity(smsIntent)
            } catch (ex: android.content.ActivityNotFoundException) {

            }
        }

        tvEdit.setOnClickListener {
            val intent = Intent(this, CustomerItemAddUpdateRemove::class.java)
            val bundle = Bundle()

            intent.putExtra("action", action)
            bundle.putParcelable("customerItemDetail", customerItemUpdate)
            intent.putExtra("Bundle", bundle)
            intent.putExtra("function", "update")
            startActivity(intent)
            finish()
        }

        ImBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun setMsg(): String {

        var msg =
            "Dear " + tvCustomerName.text + "\n" + "you are " + tvDate.text + " " + action + " " +
                    tvAmount.text + " from " + userInfo!!.businessName +
                    ".Detail of Amount is " + tvNoteDetail.text + "\n" +
                    tvTotalAmount.text + " " + tvCustomerAction.text

        return msg

    }
}