package com.zma.solutions.merahisaab.service

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.util.logging.Level


object ServiceBuilder {

    private const val HTTPS = "https://"
    private const val BASE_URL = "us-central1-merahisaab-3b70e.cloudfunctions.net/"
    val APPVERSION_URL = HTTPS + BASE_URL

    //use for logging response show in logcat
    var logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val okHttp = OkHttpClient.Builder().addInterceptor(logger)


    var gson = GsonBuilder()
        .setLenient()
        .create()

    private val builder = Retrofit.Builder().baseUrl(APPVERSION_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttp.build())

    private val retrofit = builder.build()

    fun <T> buildService(serviceType: Class<T>): T {
        return retrofit.create(serviceType)
    }
}