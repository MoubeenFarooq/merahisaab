package com.zma.solutions.merahisaab.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.adapter.MyCustomerListAdapter
import com.zma.solutions.merahisaab.listeners.CustomerItemClickListener
import com.zma.solutions.merahisaab.model.CustomerList
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), CustomerItemClickListener {

    lateinit var view: View
    var userInfo: UserInfo? = null
    var context: MainActivity = this
    val GET_PHONE_NUMBER = 3007
    var pDialog: SweetAlertDialog? = null
    var customerlist: MutableList<CustomerList.CustomerListItem>? = null
    var adapter: MyCustomerListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pDialog = Util.init_Progress(context)

        userInfo = PrefManger.getUser(context)
        if (userInfo != null && userInfo!!.bussinesss) {
            tvBusinessName.text = userInfo!!.businessName
            settingRecyclerView()
        }
        setFonts()
        clickListeners()
    }


    private fun setFonts() {

        Util.FONT_AWSOME(context, tvHome)
        Util.FONT_AWSOME(context, tvMenu)
        Util.FONT_AWSOME(context, tvReport)
        Util.FONT_AWSOME(context, tvSort)
        Util.FONT_AWSOME(context, tvSearch)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun clickListeners() {

        addCustomer.setOnClickListener(View.OnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.custome_add_dialog, null)

            val llAddCustomerFromContacts =
                mDialogView.findViewById(R.id.llAddCustomerFromContacts) as LinearLayout

            val llAddCustomer = mDialogView.findViewById(R.id.llAddCustomer) as LinearLayout


            val tvAddUser = mDialogView.findViewById(R.id.tvAddUser) as TextView
            val tvContact = mDialogView.findViewById(R.id.tvContact) as TextView


            Util.FONT_AWSOME(context, tvAddUser)
            Util.FONT_AWSOME(context, tvContact)

            //AlertDialogBuilder
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)

            val alertDialog = mBuilder.create()
            alertDialog.show()


            llAddCustomer.setOnClickListener {
                alertDialog.dismiss()
                gotoAddCustomerActivity()
            }

            llAddCustomerFromContacts.setOnClickListener {

                val contactPickerIntent =
                    Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
                contactPickerIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE)
                startActivityForResult(contactPickerIntent, GET_PHONE_NUMBER)
                alertDialog.dismiss()
            }


        })

        llMainBusiness.setOnClickListener {
            gotoBusinessActivity()
        }

        report.setOnClickListener {
            gotoReportActivity()
        }

        tvSearch.setOnClickListener {
            if (etSearch.visibility == View.VISIBLE) {
                etSearch.visibility = View.GONE
                tvSort.visibility = View.VISIBLE
                rlPositive.visibility = View.VISIBLE
                rlNegative.visibility = View.VISIBLE
                tvAll.visibility = View.VISIBLE
                rlSettled.visibility = View.VISIBLE
                buttonsLL.visibility = View.VISIBLE
                tvSearch.text = resources.getString(R.string.fas_search)
            } else {
                buttonsLL.visibility = View.GONE
                etSearch.visibility = View.VISIBLE
                tvSort.visibility = View.GONE
                rlPositive.visibility = View.GONE
                rlNegative.visibility = View.GONE
                tvAll.visibility = View.GONE
                rlSettled.visibility = View.GONE
                tvSearch.text = (resources.getString(R.string.fas_ic_cross))
            }
        }

        tvSort.setOnClickListener {
            showPopup(it)
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                filterRecyclerView(etSearch.text.toString().trim())
            }

            override fun afterTextChanged(s: Editable) {

            }

        })

        rlPositive.setOnClickListener {
            if (customerlist!!.isNotEmpty()) {
                setGotCustomer(customerlist!!)
            }
        }

        rlNegative.setOnClickListener {
            if (customerlist!!.isNotEmpty()) {
                setGaveCustomer(customerlist!!)
            }
        }

        tvAll.setOnClickListener {
            setAdapterOfCustomer(customerlist!!)
        }

        rlSettled.setOnClickListener {
            if (customerlist!!.isNotEmpty()) {
                setSettledCustomer(customerlist!!)
            }
        }

    }

    private fun filterRecyclerView(charText: String) {

        val charText = charText.toLowerCase()
        var filterCustomerList = ArrayList<CustomerList.CustomerListItem>()
        try {
            if (charText.isEmpty()) {
                if (customerlist != null)
                    setAdapterOfCustomer(customerlist!!)
            } else {
                var newItem = ArrayList<CustomerList.CustomerListItem>()
                for (item in customerlist!!.iterator()) {

                    val nameString = item.customerName
                    val numberString = item.customerPhoneNumber
                    if (nameString!!.toLowerCase().contains(charText) || numberString!!.toLowerCase().contains(
                            charText
                        )
                    ) {
                        newItem!!.add(item)
                    }
                }
                filterCustomerList.addAll(newItem!!)
                setAdapterOfCustomer(filterCustomerList!!)
            }
        } catch (io: java.lang.Exception) {

        }

    }

    private fun setSettledCustomer(customerlist: MutableList<CustomerList.CustomerListItem>) {
        var filterCustomerList = ArrayList<CustomerList.CustomerListItem>()
        for (i in customerlist.indices) {
            if (customerlist[i].youWillGive == "0" && customerlist[i].youWillGet == "0") {
                filterCustomerList.add(customerlist[i])
            }
        }

        setAdapterOfCustomer(filterCustomerList)
    }

    private fun setGaveCustomer(customerlist: MutableList<CustomerList.CustomerListItem>) {
        var filterCustomerList = ArrayList<CustomerList.CustomerListItem>()
        for (i in customerlist.indices) {
            if (customerlist[i].youWillGive != "0") {
                filterCustomerList.add(customerlist[i])
            }
        }

        setAdapterOfCustomer(filterCustomerList)

    }

    private fun setGotCustomer(customerlist: List<CustomerList.CustomerListItem>) {
        var filterCustomerList = ArrayList<CustomerList.CustomerListItem>()
        for (i in customerlist.indices) {
            if (customerlist[i].youWillGet != "0") {
                filterCustomerList.add(customerlist[i])
            }
        }

        setAdapterOfCustomer(filterCustomerList)
    }

    fun showPopup(v: View) {
        val popup = PopupMenu(this, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.menu_sorting, popup.menu)
        popup.setOnMenuItemClickListener {
            when (it!!.itemId) {
                R.id.action_assending -> {
                    sortingNameAscending()
                    return@setOnMenuItemClickListener true
                }
                R.id.action_decending -> {
                    sortingNameDescending()
                    return@setOnMenuItemClickListener true
                }
                R.id.action_recent -> {
                    sortingDateDescending()
                    return@setOnMenuItemClickListener true
                }
                R.id.action_oldest -> {
                    sortingDateAscending()
                    return@setOnMenuItemClickListener true
                }
            }
            return@setOnMenuItemClickListener false
        }
        popup.gravity = Gravity.END
        popup.show()
    }

    private fun sortingDateDescending() {
        var sortedList = customerlist!!.sortedByDescending { it.createDateAndTime }
        var mutableSorted: MutableList<CustomerList.CustomerListItem> =
            sortedList as MutableList<CustomerList.CustomerListItem>
        setAdapterOfCustomer(mutableSorted!!)
    }

    private fun sortingDateAscending() {
        var sortedList = customerlist!!.sortedWith(compareBy { it.createDateAndTime })
        var mutableSorted: MutableList<CustomerList.CustomerListItem> =
            sortedList as MutableList<CustomerList.CustomerListItem>
        setAdapterOfCustomer(mutableSorted!!)
    }

    private fun sortingNameDescending() {
        var sortedList = customerlist!!.sortedByDescending { it.customerName }
        var mutableSorted: MutableList<CustomerList.CustomerListItem> =
            sortedList as MutableList<CustomerList.CustomerListItem>
        setAdapterOfCustomer(mutableSorted!!)
    }

    private fun sortingNameAscending() {
        var sortedList = customerlist!!.sortedWith(compareBy { it.customerName })
        var mutableSorted: MutableList<CustomerList.CustomerListItem> =
            sortedList as MutableList<CustomerList.CustomerListItem>
        setAdapterOfCustomer(mutableSorted!!)
    }

    private fun gotoReportActivity() {
        val intent = Intent(this, GenerateWholeReport::class.java)
        startActivity(intent)

    }

    private fun gotoBusinessActivity() {
        val intent = Intent(this, BusinessActivity::class.java)
        startActivity(intent)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GET_PHONE_NUMBER ->
                // This is the standard resultCode that is sent back if the
                // activity crashed or didn't doesn't supply an explicit result.
                if (resultCode === Activity.RESULT_CANCELED) {
                    Toast.makeText(this, "No phone number found", Toast.LENGTH_SHORT).show()
                } else {

                    try {
                        var cursor: Cursor?
                        var phoneNumber: String?
                        var Name: String?

                        val uri = data!!.getData()
                        cursor = contentResolver.query(uri!!, null, null, null, null)
                        cursor!!.moveToFirst()

                        val phoneIndex =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)

                        val nameIndex =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)

                        phoneNumber = cursor.getString(phoneIndex)
                        Name = cursor.getString(nameIndex)

                        if (Name.isNotEmpty()) {
                            pDialog!!.show()
                            setCustomerDataOnServer(Name, phoneNumber)
                        } else {
                            Util.showToast(resources.getString(R.string.add_customer), view)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
        }
    }

    private fun gotoAddCustomerActivity() {
        val intent = Intent(this, AddCustomerActivity::class.java)
        intent.putExtra("action", "add")
        startActivity(intent)

    }

    private fun settingRecyclerView() {

        val layoutManger = LinearLayoutManager(this)
        layoutManger.orientation = LinearLayoutManager.VERTICAL
        listCustomer.layoutManager = layoutManger
        pDialog!!.show()
        getCustomerList()
    }

    private fun getCustomerList() {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.getAllCustomerFromServer(DataBinding.getAllCustomers(userInfo))

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        updateData()
                        if (it.customerList!!.size > 0) {
                            customerlist = it.customerList

                            setMainView(it.customerList!!)
                            setAdapterOfCustomer(it.customerList!!)
                        }
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )

    }

    private fun setMainView(customerList: MutableList<CustomerList.CustomerListItem>) {
        tvCustomerCount.text =
            "0" + customerList!!.size.toString() + " customer"

        settingMainGetAndGot(customerList)
    }

    private fun updateData() {
        userInfo = PrefManger.getUser(context)
        tvBusinessName.text = userInfo!!.businessName
    }

    private fun setCustomerDataOnServer(name: String, number: String) {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.addCustomerToServer(
                DataBinding.addCustomer(
                    userInfo,
                    name,
                    number,
                    Util.pick_date()
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        getCustomerList()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }

    private fun setAdapterOfCustomer(customerList: MutableList<CustomerList.CustomerListItem>) {

        adapter = MyCustomerListAdapter(context, customerList, this)
        listCustomer.adapter = adapter
    }

    private fun settingMainGetAndGot(customerList: List<CustomerList.CustomerListItem>) {
        var totalGetAmount = 0
        var totalGiveAmount = 0

        for (i in customerList.indices) {
            totalGetAmount += customerList[i].youWillGet!!.toInt()
        }

        for (i in customerList.indices) {
            totalGiveAmount += customerList[i].youWillGive!!.toInt()
        }

        tvMainGet.text = totalGetAmount.toString()
        tvMainGot.text = totalGiveAmount.toString()
    }

    override fun customerItem(customerItem: CustomerList.CustomerListItem) {
        gotoCustomerItemDetailActivity(customerItem)
    }

    private fun gotoCustomerItemDetailActivity(customerItem: CustomerList.CustomerListItem) {

        val intent = Intent(this, CustomerItemDetailActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable("customerItem", customerItem)
        intent.putExtra("Bundle", bundle)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()

        userInfo = PrefManger.getUser(context)

        if (userInfo!!.businessId != "") {
            pDialog!!.show()
            getCustomerList()
        } else {
            finish()
        }

    }
}

