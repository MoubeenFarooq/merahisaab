package com.zma.solutions.merahisaab.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_number_verification.*
import java.util.concurrent.TimeUnit


class NumberVerificationActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private var mVerificationId: String = ""
    private var mIsOtpCall: Boolean = false
    var context: NumberVerificationActivity = this
    lateinit var view: View
    var pDialog: SweetAlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.zma.solutions.merahisaab.R.layout.activity_number_verification)

        mAuth = FirebaseAuth.getInstance()

        ccp.registerCarrierNumberEditText(etPhoneNumber)

        pDialog = Util.init_Progress(context)

        Util.FONT_AWSOME(context, ImBack)

        clickListeners()
    }

    private fun clickListeners() {

        btnConfirm.setOnClickListener(View.OnClickListener {
            view = it
            if (!mIsOtpCall) {
                btnConfirmNumberActionPerform()
            } else {
                btnConfirmOtp()
            }
        })

        etPhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                checkEditedValue(s)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkEditedValue(s)
                if (s.isNotEmpty()) {
                    getFormattedFullNumber()
                }
            }

            override fun afterTextChanged(s: Editable) {
                checkEditedValue(s)
            }

        })

        etOtpCode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                checkEditedValue(s)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkEditedValue(s)
            }

            override fun afterTextChanged(s: Editable) {
                checkEditedValue(s)
            }

        })

        ImBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun btnConfirmOtp() {
        val userNumber: String = etOtpCode.text.toString()

        if (userNumber!!.isNotEmpty()) {
            verifyVerificationCode(userNumber)
        } else {
            Util.showToast(
                resources.getString(com.zma.solutions.merahisaab.R.string.otp_not_empty),
                view
            )
        }
    }

    private fun btnConfirmNumberActionPerform() {

        val userNumber: String = etPhoneNumber.text.toString()

        val validNumber: Boolean = ccp.isValidFullNumber()

        if (validNumber && userNumber!!.isNotEmpty()) {
            viewVisibleForOTP()
            sendVerificationCode(ccp.selectedCountryCodeWithPlus + "" + userNumber)
        } else {
            Util.showToast(
                resources.getString(com.zma.solutions.merahisaab.R.string.number_not_empty),
                view
            )
        }
    }

    private fun viewVisibleForOTP() {
        rlOtpCode.visibility = View.VISIBLE
        rlPhoneNumber.visibility = View.GONE
        etPhoneNumber.alpha = 0.4f
        mIsOtpCall = true
    }

    private fun sendVerificationCode(mobile: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            mobile,
            60,
            TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD,
            mCallbacks
        )
    }


    private val mCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
            //Getting the code sent by SMS
            val code = phoneAuthCredential.smsCode
            if (code != null) {
                etOtpCode.setText(code)
                //verifying the code
                verifyVerificationCode(code)
            } else {
                pDialog!!.show()
                signInWithPhoneAuthCredential(phoneAuthCredential)
            }
        }

        override fun onVerificationFailed(e: FirebaseException) {
            viewHideForOTP()
            Util.showToast(e.message.toString(), view)
        }

        override fun onCodeSent(
            s: String,
            forceResendingToken: PhoneAuthProvider.ForceResendingToken
        ) {
            super.onCodeSent(s, forceResendingToken)
            mVerificationId = s
        }
    }

    private fun viewHideForOTP() {

        etPhoneNumber.alpha = 1f
        mIsOtpCall = false
        rlOtpCode.visibility = View.GONE
        rlPhoneNumber.visibility = View.VISIBLE
    }


    private fun verifyVerificationCode(code: String) {
        //creating the credential
        val credential = PhoneAuthProvider.getCredential(mVerificationId, code)
        pDialog!!.show()
        //signing the user
        signInWithPhoneAuthCredential(credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener(this@NumberVerificationActivity,
                OnCompleteListener<AuthResult> { task ->
                    if (task.isSuccessful) {
                        saveDataAndMoveToNextActivity()
                    } else {
                        pDialog!!.hide()
                        //verification unsuccessful.. display an error message
                        var message = "Something is wrong, we will fix it soon..."
                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            message = "Invalid code entered..."
                        }

                        Util.showToast(message, view)
                    }
                })
    }

    private fun saveDataAndMoveToNextActivity() {

        var userinfo = UserInfo()
        var currentuser = FirebaseAuth.getInstance().currentUser!!.uid
        var phoneNumber = FirebaseAuth.getInstance().currentUser!!.phoneNumber
        userinfo.userId = currentuser
        userinfo.userPhone = phoneNumber
        userinfo.bussinesss = false
        userinfo.messageType = "1"

        addUserToServer(userinfo)
    }

    private fun addUserToServer(userinfo: UserInfo) {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.setUserDataOnServer(DataBinding.addUser(userinfo, Util.pick_date()))

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status == true) {
                        if (it.businessList!!.size > 0) {
                            userinfo.bussinesss = true
                        }
                        PrefManger!!.saveUser(userinfo, context)
                        gotoNextActivity()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }

    private fun gotoNextActivity() {
        val intent =
            Intent(this@NumberVerificationActivity, BusinessActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun checkEditedValue(s: CharSequence) {
        if (s.isEmpty()) {
            btnConfirm.alpha = 0.4f
        } else {
            btnConfirm.alpha = 1f
        }
    }


    fun getFormattedFullNumber(): String {
        val formattedFullNumber: String
        if (etPhoneNumber.text.isNullOrEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                formattedFullNumber =
                    PhoneNumberUtils.formatNumber(ccp.fullNumberWithPlus, ccp.defaultCountryCode)
            } else {
                formattedFullNumber = PhoneNumberUtils.formatNumber(ccp.fullNumberWithPlus)
            }
        } else {
            formattedFullNumber = "" //getSelectedCountry().getPhoneCode();
        }
        return formattedFullNumber
    }
}