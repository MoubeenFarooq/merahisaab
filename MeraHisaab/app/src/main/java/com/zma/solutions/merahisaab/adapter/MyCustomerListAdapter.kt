package com.zma.solutions.merahisaab.adapter


import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.listeners.CustomerItemClickListener
import com.zma.solutions.merahisaab.model.CustomerList
import kotlinx.android.synthetic.main.adapter_customer_item_main.view.*

class MyCustomerListAdapter(
    val context: Context,
    val customerList: MutableList<CustomerList.CustomerListItem>,
    val customerItemClickListener: (CustomerItemClickListener)
) : RecyclerView.Adapter<MyCustomerListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.adapter_customer_item_main, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return customerList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val hobbyItem = customerList[position]
        holder.setDate(hobbyItem, position)
    }


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var currentItem: CustomerList.CustomerListItem? = null

        init {
            itemView.llCustomerItemMain.setOnClickListener(View.OnClickListener {
                customerItemClickListener!!.customerItem(currentItem!!)
            })
        }


        fun setDate(
            customerItem: CustomerList.CustomerListItem?,
            pos: Int
        ) {//? and !! use for not null
            itemView.tvCustomerName.text = customerItem!!.customerName
            itemView.tvCustomerDate.text = customerItem!!.createDateAndTime

            when {

                customerItem.youWillGet != "0" -> {

                    itemView.tvCustomerAmount.text = customerItem!!.youWillGet
                    itemView.tvCustomerAmountTitle.text = "You Will Get"
                    itemView.tvCustomerAmount.setTextColor(Color.parseColor("#00695C"))

                }

                customerItem.youWillGive != "0" -> {

                    itemView.tvCustomerAmount.text = customerItem!!.youWillGive
                    itemView.tvCustomerAmountTitle.text = "You Will Give"
                    itemView.tvCustomerAmount.setTextColor(Color.parseColor("#921b38"))

                }

                else -> {

                    itemView.tvCustomerAmount.text = customerItem!!.youWillGive
                    itemView.tvCustomerAmountTitle.text = "Settled Up"
                    itemView.tvCustomerAmount.setTextColor(Color.parseColor("#888888"))

                }

            }

            this.currentItem = customerItem

        }

    }

}