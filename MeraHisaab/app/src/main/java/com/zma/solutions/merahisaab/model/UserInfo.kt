package com.zma.solutions.merahisaab.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UserInfo {
    @SerializedName("userId")
    @Expose
    var userId: String? = null

    @SerializedName("UserPhone")
    @Expose
    var userPhone: String? = null

    @SerializedName("MessageType")
    @Expose
    var messageType: String? = null

    @SerializedName("status")
    @Expose
    var status: Boolean = false

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("business")
    @Expose
    var bussinesss: Boolean = false

    @SerializedName("BusinessId")
    @Expose
    var businessId: String? = null

    @SerializedName("BusinessName")
    @Expose
    var businessName: String? = null


    @SerializedName("businessList")
    @Expose
    var businessList: List<BusinessItem>? = null

    inner class BusinessItem {

        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("Date")
        @Expose
        var date: String? = null

        @SerializedName("Time")
        @Expose
        var time: String? = null

        @SerializedName("BusinessName")
        @Expose
        var businessName: String? = null

        @SerializedName("UserId")
        @Expose
        var userId: String? = null

    }


}
