package com.zma.solutions.merahisaab.listeners

import com.zma.solutions.merahisaab.model.CustomerItemDetail

interface CustomerItemDetailClickListener {
    fun customerDetailItem(customerDetailItem: CustomerItemDetail.CustomerDetailListItem)
}