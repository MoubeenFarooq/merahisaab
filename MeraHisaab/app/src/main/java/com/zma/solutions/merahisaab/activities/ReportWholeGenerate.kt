package com.zma.solutions.merahisaab.activities

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.FileProvider
import com.tejpratapsingh.pdfcreator.activity.PDFCreatorActivity
import com.tejpratapsingh.pdfcreator.utils.PDFUtil
import com.tejpratapsingh.pdfcreator.views.PDFBody
import com.tejpratapsingh.pdfcreator.views.PDFHeaderView
import com.tejpratapsingh.pdfcreator.views.PDFTableView
import com.tejpratapsingh.pdfcreator.views.basic.PDFHorizontalView
import com.tejpratapsingh.pdfcreator.views.basic.PDFImageView
import com.tejpratapsingh.pdfcreator.views.basic.PDFLineSeparatorView
import com.tejpratapsingh.pdfcreator.views.basic.PDFTextView
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.model.ReportData
import com.zma.solutions.merahisaab.model.UserInfo
import kotlinx.android.synthetic.main.activity_adding_update_item.*
import java.io.File
import java.net.URLConnection


class ReportWholeGenerate : PDFCreatorActivity() {

    var context: ReportWholeGenerate = this
    var reportMain: ReportData? = null
    var reportList: List<ReportData.ReportItemDetail>? = null
    var userInfo: UserInfo? = null

    var businessName = ""
    var userNumber = ""
    var totalGot = ""
    var totalGave = ""
    var totalNet = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getDataFromPreviousActivity()

        Util.FONT_AWSOME(context, ImBack)


    }

    private fun getDataFromPreviousActivity() {
        userInfo = PrefManger.getUser(context)

        val bundle = intent.getBundleExtra("Bundle")
        reportMain =
            bundle.getParcelable("reportList")//its mean not null

        reportList = reportMain!!.reportItemDetail

        userNumber = userInfo!!.userPhone.toString()

        businessName = userInfo!!.businessName.toString()

        settingMainGetAndGot(reportList)

    }


    private fun settingMainGetAndGot(customerList: List<ReportData.ReportItemDetail>?) {
        var totalGetAmount = 0
        var totalGiveAmount = 0

        for (i in customerList!!.indices) {
            totalGetAmount += customerList[i].youGot!!.toInt()
        }

        for (i in customerList.indices) {
            totalGiveAmount += customerList[i].youGave!!.toInt()
        }

        totalGot = totalGetAmount.toString()
        totalGave = totalGiveAmount.toString()

        totalNet = if (totalGave.toInt() > totalGot.toInt()) {
            ((totalGave.toInt() - totalGot.toInt()).toString() + " (You Will Give)")
        } else {
            ((totalGot.toInt() - totalGave.toInt()).toString() + " (You Will Get)")
        }

        callForPdf()
    }

    private fun callForPdf() {

        createPDF("Transaction  Detail " + Util.pick_date(), object : PDFUtil.PDFUtilListener {
            override fun pdfGenerationSuccess(savedPDFFile: File) {
                Toast.makeText(this@ReportWholeGenerate, "PDF Created", Toast.LENGTH_SHORT).show()
            }

            override fun pdfGenerationFailure(exception: Exception) {
                Toast.makeText(this@ReportWholeGenerate, "PDF NOT Created", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }

    override fun onNextClicked(savedPDFFile: File?) {
        val intentShareFile = Intent(Intent.ACTION_SEND)

        val apkURI: Uri = FileProvider.getUriForFile(
            applicationContext,
            applicationContext
                .packageName + ".provider", savedPDFFile!!
        )
        intentShareFile.setDataAndType(
            apkURI,
            URLConnection.guessContentTypeFromName(savedPDFFile.name)
        )
        intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        intentShareFile.putExtra(
            Intent.EXTRA_STREAM,
            Uri.parse("file://" + savedPDFFile.absolutePath)
        )

        startActivity(Intent.createChooser(intentShareFile, "Share File"))
    }

    override fun getHeaderView(page: Int): PDFHeaderView {
        val headerView = PDFHeaderView(applicationContext)

        val pdfTextViewPage = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
        pdfTextViewPage.setText("Mera Hisaab")
        pdfTextViewPage.setLayout(
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, 0f
            )
        )
        pdfTextViewPage.view.gravity = Gravity.CENTER_HORIZONTAL

        headerView.addView(pdfTextViewPage)

        val horizontalView = PDFHorizontalView(applicationContext)

        val pdfTextView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.HEADER)
        val word = SpannableString("INVOICE")
        word.setSpan(
            ForegroundColorSpan(Color.DKGRAY),
            0,
            word.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        pdfTextView.text = word
        pdfTextView.setLayout(
            LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1f
            )
        )
        pdfTextView.view.gravity = Gravity.CENTER_VERTICAL
        pdfTextView.view.setTypeface(pdfTextView.view.typeface, Typeface.BOLD)

        horizontalView.addView(pdfTextView)

        var imageView = PDFImageView(applicationContext)
        var imageLayoutParam = LinearLayout.LayoutParams(
            60,
            60, 0f
        )
        imageView.setImageScale(ImageView.ScaleType.CENTER_INSIDE)
        imageView.setImageResource(R.mipmap.logo_image)
        imageLayoutParam.setMargins(0, 0, 10, 0)
        imageView.setLayout(imageLayoutParam)

        horizontalView.addView(imageView)

        headerView.addView(horizontalView)

        val lineSeparatorView1 =
            PDFLineSeparatorView(applicationContext).setBackgroundColor(Color.WHITE)
        headerView.addView(lineSeparatorView1)

        return headerView
    }

    override fun getBodyViews(): PDFBody {
        val pdfBody = PDFBody()

        val pdfCompanyNameView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.HEADER)
        pdfCompanyNameView.setText("Business Name: $businessName")
        pdfBody.addView(pdfCompanyNameView)

        val pdfAddressView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
        pdfAddressView.setText("Business Number: $userNumber")
        pdfBody.addView(pdfAddressView)

        val pdfNoOfTranscationView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
        pdfNoOfTranscationView.setText("No of Transaction: ${reportList!!.size}")
        pdfBody.addView(pdfNoOfTranscationView)


        val pdfTotalCreditView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
        pdfTotalCreditView.setText("Total Gave: $totalGave")
        pdfTotalCreditView.setTextColor(resources.getColor(R.color.diOrange))
        pdfBody.addView(pdfTotalCreditView)

        val pdfTotalDebitView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
        pdfTotalDebitView.setText("Total Got: $totalGot")
        pdfTotalCreditView.setTextColor(resources.getColor(R.color.diGreen))
        pdfBody.addView(pdfTotalDebitView)

        val pdfNetView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
        pdfNetView.setText("Net : $totalNet")
        pdfBody.addView(pdfNetView)

        val textInTable = arrayOf("0", "1", "2", "3")

        val lineSeparatorView2 =
            PDFLineSeparatorView(applicationContext).setBackgroundColor(Color.WHITE)
        pdfBody.addView(lineSeparatorView2)


        val tableHeader = PDFTableView.PDFTableRowView(applicationContext)
        for (s in textInTable) {
            val pdfTextView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H1)
            when (s) {
                "0" -> pdfTextView.setText("Sr")
                "1" -> pdfTextView.setText("Date")
                "2" -> pdfTextView.setText("You Gave")
                else -> pdfTextView.setText("You Got")
            }

            tableHeader.addToRow(pdfTextView)
        }


        val tableRowView1 = PDFTableView.PDFTableRowView(applicationContext)
        for (s in textInTable) {
            val pdfTextView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H2)
            when (s) {
                "0" -> pdfTextView.setText((s.toInt() + 1).toString())
                "1" -> pdfTextView.setText(reportList!![0].dateCreated.toString())
                "2" -> pdfTextView.setText(reportList!![0].youGave).setTextColor(
                    resources.getColor(
                        R.color.diOrange
                    )
                )
                else -> pdfTextView.setText(reportList!![0].youGot).setTextColor(
                    resources.getColor(
                        R.color.diGreen
                    )
                )
            }
            tableRowView1.addToRow(pdfTextView)
        }

        val tableView = PDFTableView(applicationContext, tableHeader, tableRowView1)
        for (i in 1 until reportList!!.size) {
            // Create 10 rows
            val tableRowView = PDFTableView.PDFTableRowView(applicationContext)
            for (s in textInTable) {
                val pdfTextView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H2)
                when (s) {
                    "0" -> pdfTextView.setText((i + 1).toString()).paddingTop = 10
                    "1" -> pdfTextView.setText(reportList!![i].dateCreated)
                    "2" -> pdfTextView.setText(reportList!![i].youGave).setTextColor(
                        resources.getColor(R.color.diOrange)
                    )
                    else -> pdfTextView.setText(reportList!![i].youGot).setTextColor(
                        resources.getColor(R.color.diGreen)
                    )
                }
                tableRowView.addToRow(pdfTextView)
            }
            tableView.addRow(tableRowView)
        }
        pdfBody.addView(tableView)

        val lineSeparatorView3 =
            PDFLineSeparatorView(applicationContext).setBackgroundColor(Color.BLACK)
        pdfBody.addView(lineSeparatorView3)


        val pdfDateView = PDFTextView(applicationContext, PDFTextView.PDF_TEXT_SIZE.H2)
        pdfDateView.setText("Print out: " + Util.pick_date() + "                  " + "Auto Generated")

        pdfBody.addView(pdfDateView)

        return pdfBody
    }


}
