package com.zma.solutions.merahisaab.Utils


import android.content.Context
import com.google.gson.Gson
import android.content.SharedPreferences
import com.zma.solutions.merahisaab.model.UserInfo


object PrefManger {

    private val PREF_NAME = "PrefManager"
    private val TAG = "PrefManger"
    private val USER = "user"
    private lateinit var sharedPreferences: SharedPreferences

    fun saveUser(user: UserInfo?, context: Context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = gson.toJson(user)

        val editor = sharedPreferences!!.edit()
        editor.putString(USER, json)
        editor.commit()

    }

    fun getUser(context: Context): UserInfo? {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val gson = Gson()
        val str = sharedPreferences!!.getString(USER, "")

        return if (str != "") {
            gson.fromJson<UserInfo>(str, UserInfo::class.java)
        } else {
            null
        }
    }

    fun clearPreferences(context: Context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        sharedPreferences!!.edit().clear().commit()
    }

}