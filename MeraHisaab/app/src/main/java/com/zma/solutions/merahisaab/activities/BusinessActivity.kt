package com.zma.solutions.merahisaab.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.adapter.MyBussinessListAdapter
import com.zma.solutions.merahisaab.listeners.BussinessItemClickListener
import com.zma.solutions.merahisaab.model.BusinessList
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_create_business.*

class BusinessActivity : AppCompatActivity(),
    BussinessItemClickListener {

    lateinit var view: View
    var context: BusinessActivity = this

    var userInfo: UserInfo? = null
    var action = ""
    var bId = ""
    var businessListMain: List<BusinessList.BusinessItem>? = null
    var business_deleted = ""
    var pDialog: SweetAlertDialog? = null
    var isCreateBusinessShowing = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_business)

        Util.FONT_AWSOME(context, ImBack)

        pDialog = Util.init_Progress(context)

        userInfo = PrefManger.getUser(context)
        if (userInfo != null && userInfo!!.bussinesss) {
            setViewWhenBusinessAdded()
        } else {
            setViewWhenNoBusiness("no")
        }

        clickListeners()
    }

    private fun settingRecyclerView() {

        val layoutManger = LinearLayoutManager(this)
        layoutManger.orientation = LinearLayoutManager.VERTICAL
        listBusiness.layoutManager = layoutManger

        if (Util.DETECT_INTERNET_CONNECTION(context)) {
            pDialog!!.show()
            getAllBussiness()
        } else {
            Util.showDialog("Alert", "No Internet Connection", context)
        }
    }

    private fun setAdapterOfBussiness(businessList: List<BusinessList.BusinessItem>) {
        val adapter = MyBussinessListAdapter(context, businessList, this)
        listBusiness.adapter = adapter
    }

    private fun setViewWhenBusinessAdded() {
        addBusiness.visibility = View.VISIBLE
        listBusiness.visibility = View.VISIBLE
        rlSave.visibility = View.GONE
        llBusinessName.visibility = View.GONE
        settingRecyclerView()
    }

    private fun setViewWhenNoBusiness(action: String) {
        if (action.equals("no")) {
            ImBack.visibility = View.GONE
        }
        hideBusinessList()
    }

    private fun clickListeners() {

        btnSave.setOnClickListener(View.OnClickListener {

            if (Util.DETECT_INTERNET_CONNECTION(context)) {
                if (etBusinessName.text.isNotEmpty()) {
                    var businessName: String = etBusinessName.text.toString()
                    pDialog!!.show()
                    if (action == "update") {
                        updateBusinessWebservice(businessName)
                    } else {
                        saveBusinessWebservice(businessName)
                    }
                } else {
                    Util.showToast(resources.getString(R.string.enter_business_name), view)
                }
            } else {
                Util.showToast("No Internet Connection", it)
            }
        })

        createBusiness.setOnClickListener {
            setViewWhenNoBusiness("yes")

        }

        ImBack.setOnClickListener {

            if (isCreateBusinessShowing) {
                showBusinessList()
            }else {
                onBackPressed()
            }

        }

    }


    private fun gotoNextActivity(businessItem: String?, businessName: String?) {

        updateUserInfo(businessItem, businessName)
        val intent = Intent(this@BusinessActivity, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun updateUserInfo(businessItem: String?, businessName: String?) {

        userInfo!!.businessId = businessItem
        userInfo!!.businessName = businessName
        userInfo!!.bussinesss = true
        PrefManger.saveUser(userInfo, context)
    }

    override fun businessItem(businessItem: BusinessList.BusinessItem) {
        gotoNextActivity(businessItem.id, businessItem.businessName)
    }

    override fun businessItemUpdate(businessItem: BusinessList.BusinessItem) {
        action = "update"
        bId = businessItem!!.id.toString()
        setViewWhenNoBusiness("no")
        etBusinessName.setText(businessItem.businessName)
    }


    private fun saveBusinessWebservice(businessName: String) {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.addBusinessToServer(
                DataBinding.addBusiness(
                    userInfo,
                    businessName,
                    Util.pick_date()
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status == true) {

                        gotoNextActivity(it.businessId, businessName)
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }

    private fun getAllBussiness() {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.getAllBusinessFromServer(DataBinding.getAllBusiness(userInfo))

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        businessListMain = it.businessList
                        if (businessListMain!!.isNotEmpty()) {
                            if (business_deleted == "true") {
                                updateUserInfo(
                                    businessListMain!!.get(0).id,
                                    businessListMain!!.get(0).businessName
                                )
                            }
                            setAdapterOfBussiness(businessListMain!!)
                        } else {
                            clearBusinessData()
                            setViewWhenNoBusiness("no")
                        }
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }

    private fun clearBusinessData() {

        userInfo = PrefManger.getUser(context)

        userInfo!!.businessId = ""
        userInfo!!.businessName = ""
        userInfo!!.bussinesss = false
        PrefManger.saveUser(userInfo, context)


    }


    private fun updateBusinessWebservice(businessName: String) {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.updateBusinessToServer(
                DataBinding.updateBusiness(
                    bId,
                    businessName,
                    Util.pick_date()
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status == true) {

                        if (userInfo!!.businessId.equals(bId)) {
                            updateUserInfo(bId, businessName)
                        }
                        setViewWhenBusinessAdded()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )

    }

    override fun businessItemDelete(businessItem: BusinessList.BusinessItem) {
        pDialog!!.show()
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.deleteBusinessToServer(
                DataBinding.deleteBusiness(
                    businessItem.id
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    if (it.status) {
                        if (userInfo!!.businessId.equals(businessItem.id)) {
                            business_deleted = "true"
                        }
                        getAllBussiness()
                    } else {
                        pDialog!!.hide()
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }
    private fun showBusinessList(){
        rlSave.visibility = View.GONE
        llBusinessName.visibility = View.GONE
        addBusiness.visibility = View.VISIBLE
        listBusiness.visibility = View.VISIBLE
        header_name.text = resources.getString(R.string.business_list)
        isCreateBusinessShowing = false
    }
    private fun hideBusinessList(){
        rlSave.visibility = View.VISIBLE
        llBusinessName.visibility = View.VISIBLE
        addBusiness.visibility = View.GONE
        listBusiness.visibility = View.GONE
        header_name.text = resources.getString(R.string.create_new_business)
        isCreateBusinessShowing = true
    }

    override fun onBackPressed() {

        if (isCreateBusinessShowing) {
            showBusinessList()
        }else {
            if (businessListMain != null && businessListMain!!.isNotEmpty()) {
                super.onBackPressed()
            } else {
                finish()
            }
        }


    }

}