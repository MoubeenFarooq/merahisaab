package com.zma.solutions.merahisaab.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.model.CustomerList
import com.zma.solutions.merahisaab.model.UserInfo
import kotlinx.android.synthetic.main.activity_detail_share.tvAmount
import kotlinx.android.synthetic.main.activity_detail_share.tvCustomerName
import kotlinx.android.synthetic.main.activity_detail_share.tvMessage
import kotlinx.android.synthetic.main.activity_detail_share.tvWhatsapp
import kotlinx.android.synthetic.main.activity_item_share.*

class CustomerItemShare : AppCompatActivity() {

    lateinit var view: View
    val context: CustomerItemShare = this
    var customerItem: CustomerList.CustomerListItem? = null

    var userInfo: UserInfo? = null
    var action = ""
    var businessId = ""
    var function = ""
    var pDialog: SweetAlertDialog? = null

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_share)

        userInfo = PrefManger.getUser(context)

        pDialog = Util.init_Progress(context)

        setFontAwsome()
        getDataFromPreviousActivity()

        clickListeners()
    }

    private fun setFontAwsome() {
        Util.FONT_AWSOME(context, imBack)
        Util.FONT_AWSOME(context, tvWhatsapp)
        Util.FONT_AWSOME(context, tvMessage)
    }


    private fun getDataFromPreviousActivity() {

        val bundle = intent.getBundleExtra("Bundle")

        customerItem = bundle.getParcelable("customerItem")//its mean not null


        if (customerItem!!.youWillGive.equals("0")) {
            action = "get"
        } else {
            action = "give"
        }

        if (action == "get") {
            tvAction.text = "You Will Get"
            tvAmount.text = "Rs. " + customerItem!!.youWillGet
            tvAmount.setTextColor(resources.getColor(R.color.diGreen))

        } else {
            tvAction.text = "You Will Give"
            tvAmount.text = "Rs. " + customerItem!!.youWillGive
            tvAmount.setTextColor(resources.getColor(R.color.diOrange))
        }

        tvCustomerName.text = customerItem!!.customerName
        tvCustomerNumber.text = customerItem!!.customerPhoneNumber

        tvMsg.text = setMsg()

    }


    private fun clickListeners() {
        tvWhatsapp.setOnClickListener {
            var msg = setMsg()

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.setPackage("com.whatsapp")
            intent.putExtra(Intent.EXTRA_TEXT, msg)
            if (intent.resolveActivity(packageManager) == null) {
                Util.showToast("Please Install WhatsApp", it)
            } else {
                startActivity(intent)
            }
        }

        tvMessage.setOnClickListener {
            var msg = setMsg()
            val smsIntent = Intent(Intent.ACTION_VIEW)

            smsIntent.data = Uri.parse("smsto:")
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("address", customerItem!!.customerPhoneNumber)
            smsIntent.putExtra("sms_body", msg)

            try {
                startActivity(smsIntent)
            } catch (ex: android.content.ActivityNotFoundException) {

            }
        }


        imBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun setMsg(): String {

        var msg =
            "Payment Reminder " + "\n" + "From: " + userInfo!!.businessName + "\n" + "To: " + tvCustomerName.text + "\n" +
                    "Balance: " + tvAmount.text + "\n" + "is due at " + Util.pick_date() + " Please make your payment as soon as possible."

        return msg

    }
}