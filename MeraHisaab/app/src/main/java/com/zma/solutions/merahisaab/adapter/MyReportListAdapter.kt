package com.zma.solutions.merahisaab.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.activities.GenerateSingleReport
import com.zma.solutions.merahisaab.model.CustomerItemDetail
import kotlinx.android.synthetic.main.adapter_report.view.*

class MyReportListAdapter(
    var context: GenerateSingleReport,
    var customerList: List<CustomerItemDetail.CustomerDetailListItem>
) : RecyclerView.Adapter<MyReportListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.adapter_report, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return customerList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val hobbyItem = customerList[position]
        holder.setDate(hobbyItem, position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var currentItem: CustomerItemDetail.CustomerDetailListItem? = null


        fun setDate(
            customerItem: CustomerItemDetail.CustomerDetailListItem,
            pos: Int
        ) {//? and !! use for not null
            itemView.tvItemDetailDate.text =
                customerItem!!.dateCreated!!
            itemView.tvItemDetail.text =
                customerItem!!.details!!
            when {
                customerItem.youGot != "0" -> {
                    itemView.tvItemDetailYouGot.text = customerItem!!.youGot
                }
                customerItem.youGave != "0" -> {
                    itemView.tvItemDetailYouGive.text = customerItem!!.youGave
                }
            }

            this.currentItem = customerItem
        }
    }

}
