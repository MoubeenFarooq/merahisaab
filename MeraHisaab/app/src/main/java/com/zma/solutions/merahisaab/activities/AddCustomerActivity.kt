package com.zma.solutions.merahisaab.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.model.CustomerList
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_customer_add.*

class AddCustomerActivity : AppCompatActivity() {

    lateinit var view: View
    var userInfo: UserInfo? = null
    var context: AddCustomerActivity = this
    var customerItem: CustomerList.CustomerListItem? = null
    var pDialog: SweetAlertDialog? = null
    var action = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_add)
        userInfo = PrefManger.getUser(context)
        Util.FONT_AWSOME(context, ImBack)

        pDialog = Util.init_Progress(context)

        getDataFromPreviousActivity()
        setClickListeners()
    }

    private fun getDataFromPreviousActivity() {
        action = intent.getStringExtra("action")

        if (action == "update") {
            val bundle = intent.getBundleExtra("Bundle")
            customerItem =
                bundle.getParcelable("customerItem")//its mean not null
            etCustomerName.setText(customerItem!!.customerName)
            etCustomerPhoneNumber.setText(customerItem!!.customerPhoneNumber)
            btnSave.text = resources.getString(R.string.update)
        }

    }

    private fun setClickListeners() {

        btnSave.setOnClickListener {

            if (Util.DETECT_INTERNET_CONNECTION(context)) {

                if (etCustomerName.text.isNotEmpty()) {
                    var name: String = etCustomerName.text.toString()
                    var number: String = etCustomerPhoneNumber.text.toString()
                    pDialog!!.show()
                    if (action == "update") {
                        updateCustomerDataOnServer(name, number, customerItem!!.id)
                    } else {
                        setCustomerDataOnServer(name, number)
                    }
                } else {
                    Util.showToast(resources.getString(R.string.enter_customer_name), view)
                }
            } else {
                Util.showToast("No Internet Connection", it)
            }
        }

        ImBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun updateCustomerDataOnServer(
        name: String,
        number: String,
        id: String?
    ) {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.updateCustomerToServer(
                DataBinding.updateCustomer(
                    id,
                    name,
                    number,
                    Util.pick_date()
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        finish()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }

    private fun setCustomerDataOnServer(name: String, number: String) {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.addCustomerToServer(
                DataBinding.addCustomer(
                    userInfo,
                    name,
                    number,
                    Util.pick_date()
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        finish()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }
}