package com.zma.solutions.merahisaab.activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.adapter.ViewPagerAdapter
import com.zma.solutions.merahisaab.model.UserInfo
import kotlinx.android.synthetic.main.activity_start.*
import java.util.*


class SplashActivity : AppCompatActivity() {
    var context: SplashActivity = this

    val INTERNET_PERMISSION_REQUEST_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.zma.solutions.merahisaab.R.layout.activity_start)
        if (checkPermissions()) {
            nextAction()
        } else {
            requestPermissions()
        }
        setViewAndClickListener()
    }

    private fun nextAction() {
        var userInfo: UserInfo? = PrefManger.getUser(context)
        if (userInfo != null && userInfo.businessId != "") {
            gotoNextActivity()
        } else if (userInfo != null) {
            gotoBusinessActivity()
        }
    }

    private fun gotoBusinessActivity() {

        val intent = Intent(this, BusinessActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun gotoNextActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()

    }

    private fun setViewAndClickListener() {
        tvEnglish.setOnClickListener(View.OnClickListener {
            //  setViewVisibility()

            if (Util.DETECT_INTERNET_CONNECTION(context)) {
                val intent = Intent(this, NumberVerificationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Util.showToast("No Internet Connection", it)
            }
        })

        tvUrdu.setOnClickListener(View.OnClickListener {
            //  setViewVisibility()
            setProjectLanguage("ur")

            if (Util.DETECT_INTERNET_CONNECTION(context)) {
                val intent = Intent(this, NumberVerificationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Util.showToast("No Internet Connection", it)
            }
        })

        btnStart.setOnClickListener(View.OnClickListener {
            if (Util.DETECT_INTERNET_CONNECTION(context)) {
                val intent = Intent(this, NumberVerificationActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Util.showToast("No Internet Connection", it)
            }
        })
    }

    private fun setProjectLanguage(languageName: String) {

        val locale = Locale(languageName)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)


    }

    private fun setViewPager() {
        if (viewPager != null) {
            val adapter = ViewPagerAdapter(supportFragmentManager)
            viewPager.adapter = adapter
            dots_indicator.setViewPager(viewPager)
        }
    }

    private fun setViewVisibility() {
        setViewPager()
        llLanguageSelect.visibility = View.GONE
        RlStart.visibility = View.VISIBLE
    }

    fun checkPermissions(): Boolean {
        val resultSms =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
        val resultRecieveSms =
            ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
        val resultCamera =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val resultInternet =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
        val resultContact =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)

        return resultSms == PackageManager.PERMISSION_GRANTED && resultRecieveSms == PackageManager.PERMISSION_GRANTED
                && resultCamera == PackageManager.PERMISSION_GRANTED && resultInternet == PackageManager.PERMISSION_GRANTED
                && resultContact == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {

        ActivityCompat.requestPermissions(
            this, arrayOf(
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_CONTACTS
            ), INTERNET_PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            INTERNET_PERMISSION_REQUEST_CODE -> if (grantResults.size > 0) {

                val smsread = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val smsrecieve = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val camera = grantResults[2] == PackageManager.PERMISSION_GRANTED
                val internet = grantResults[3] == PackageManager.PERMISSION_GRANTED
                val contact = grantResults[4] == PackageManager.PERMISSION_GRANTED

                if (smsread && contact) {
                    nextAction()
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.INTERNET)) {
                            showMessageOKCancel(
                                DialogInterface.OnClickListener { dialog, which ->
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(
                                            arrayOf(
                                                Manifest.permission.READ_SMS,
                                                Manifest.permission.RECEIVE_SMS,
                                                Manifest.permission.CAMERA,
                                                Manifest.permission.ACCESS_NETWORK_STATE,
                                                Manifest.permission.READ_CONTACTS
                                            ), INTERNET_PERMISSION_REQUEST_CODE
                                        )
                                    }
                                })
                            return
                        }
                    }
                }
            }
        }
    }

    private fun showMessageOKCancel(onClickListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(context)
            .setMessage("You need to allow access to all the permissions")
            .setPositiveButton("OK", onClickListener)
            .setNegativeButton("Cancel", onClickListener)
            .create()
            .show()
    }
}



