package com.zma.solutions.merahisaab.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.listeners.CustomerItemDetailClickListener
import com.zma.solutions.merahisaab.model.CustomerItemDetail
import kotlinx.android.synthetic.main.adapter_item_detail.view.*

class MyCustomerListItemDetailAdapter(
    val context: Context,
    val customerDetailList: List<CustomerItemDetail.CustomerDetailListItem>,
    val customerItemClickListener: (CustomerItemDetailClickListener)
) : RecyclerView.Adapter<MyCustomerListItemDetailAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.adapter_item_detail, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return customerDetailList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val hobbyItem = customerDetailList[position]
        holder.setDate(hobbyItem, position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var currentItem: CustomerItemDetail.CustomerDetailListItem? = null

        init {
            itemView.llCustomerItemDetail.setOnClickListener(View.OnClickListener {
                customerItemClickListener!!.customerDetailItem(currentItem!!)
            })
        }

        fun setDate(
            customerItemDetail: CustomerItemDetail.CustomerDetailListItem?,
            pos: Int
        ) {//? and !! use for not null
            itemView.tvItemDetailYouGive.text = customerItemDetail!!.youGave
            itemView.tvItemDetailYouGot.text = customerItemDetail!!.youGot
            itemView.tvItemDetailDate.text =
                customerItemDetail!!.dateCreated 

            itemView.tvItemDetail.text = customerItemDetail!!.details
            this.currentItem = customerItemDetail
        }
    }

}
