package com.zma.solutions.merahisaab.Utils

object Constant {

    const val USER = "user"
    const val ADDUSER_ENDPOINT = "addUser"
    const val USERPHONENUMBER = "UserPhone"
    const val USERID = "UserId"
    const val USERNAME = "UserName"
    const val MESSAGETYPE = "MessageType"


    const val BUSINESS = "business"
    const val ADDBUSINESS_ENDPOINT = "addbusiness"
    const val GETALLBUSINESS_ENDPOINT = "getbusinessByUserId"

    const val BUSSINESS_NAME = "BusinessName"
    const val CREATE_DATE_TIME = "DateCreated"
    const val UPDATE_BUSINESS_DATE = "BusinessUpdateTime"


    const val CUSTOMER = "customer"
    const val ADD_CUSTOMER = "addCustomer"
    const val GETALLCUSTOMER_ENDPOINT = "getCustomerByUserAndBusinessId"


    const val BUSSINESS_ID = "BusinessId"
    const val CUSTOMER_PHONE_NUMBER = "CustomerPhoneNumber"
    const val CUSTOMER_NAME = "CustomerName"
    const val YOU_WILL_GIVE = "YouWillGive"
    const val YOU_WILL_GET = "YouWillGet"
    const val CUSTOMER_ADDRESS = "CustomerAddress"

    const val CUSTOMER_DETAIL = "customerDetail"
    const val GET_CUSTOMER_DETAIL = "getCustomerDetail"
    const val CUSTOMER_ID = "CustomerId"
    const val ADD_CUSTOMER_DETAIL = "addCustomerDetail"


    const val YOU_GAVE = "YouGave"
    const val YOU_GOT = "YouGot"
    const val DETAIL = "Details"
    const val DATE_CREATED = "DateCreated"
    const val RECORD_ID = "RecordId"
    const val UPDATE_CUSTOMER_DETAIL = "updateCustomerRecord"
    const val DELETE_CUSTOMER_DETAIL = "deleteCustomerRecord"
    const val DELETE_CUSTOMER = "deleteCustomerBYId"
    const val UPDATE_CUSTOMER = "updateCustomerById"
    const val UPDATE_BUSINESS = "updatebusinessById"
    const val DELETE_BUSINESS = "deleteBusinessById"

    const val REPORT = "report"
    const val GETREPORTDATA = "getReportData"



}