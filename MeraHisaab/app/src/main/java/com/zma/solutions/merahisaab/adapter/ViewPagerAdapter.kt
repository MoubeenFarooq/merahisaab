package com.zma.solutions.merahisaab.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.zma.solutions.merahisaab.fragments.FirstFragment
import com.zma.solutions.merahisaab.fragments.SecondFragment
import com.zma.solutions.merahisaab.fragments.ThirdFragment


class ViewPagerAdapter internal constructor(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val COUNT = 3


    override fun getItem(position: Int): Fragment {

        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = FirstFragment()
            1 -> fragment = SecondFragment()
            2 -> fragment = ThirdFragment()
        }
        return fragment!!

    }

    override fun getCount(): Int {
        return COUNT
    }


}
