package com.zma.solutions.merahisaab.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.gp89developers.calculatorinputview.CalculatorBuilder
import com.gp89developers.calculatorinputview.activities.CalculatorActivity
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.model.CustomerItemDetail
import com.zma.solutions.merahisaab.model.CustomerList
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.androidexception.andexalertdialog.AndExAlertDialog
import kotlinx.android.synthetic.main.activity_adding_update_item.*
import java.util.*


class CustomerItemAddUpdateRemove : AppCompatActivity() {


    var Amount: String = ""
    lateinit var view: View
    val context: CustomerItemAddUpdateRemove = this
    var customerItem: CustomerList.CustomerListItem? = null
    var customerItemUpdate: CustomerItemDetail.CustomerDetailListItem? = null
    var userInfo: UserInfo? = null
    var action = ""
    var date = ""
    var customerId = ""
    var businessId = ""
    var amountGet = "0"
    var amountGive = "0"
    var itemDetail = ""
    var function = ""
    var recordId = ""
    var pDialog: SweetAlertDialog? = null

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adding_update_item)

        userInfo = PrefManger.getUser(context)

        pDialog = Util.init_Progress(context)

        Util.FONT_AWSOME(context, ImBack)
        Util.FONT_AWSOME(context, tvCalander)
        Util.FONT_AWSOME(context, tvRemove)

        tvDate.text = Util.pick_date()

        clickListeners()

        getDataFromPreviousActivity()
    }


    private fun getDataFromPreviousActivity() {

        action = intent.getStringExtra("action")
        function = intent.getStringExtra("function")

        val bundle = intent.getBundleExtra("Bundle")

        if (function == "add") {
            tvRemove.visibility = View.GONE
            customerItem =
                bundle.getParcelable("customerItem")//its mean not null

        } else {
            customerItemUpdate =
                bundle.getParcelable("customerItemDetail")//its mean not null

            if (action == "get") {
                etAmount.setText(customerItemUpdate!!.youGot)
            } else {
                etAmount.setText(customerItemUpdate!!.youGave)
            }
            etItemDetail.setText(customerItemUpdate!!.details)
            tvDate.setText(customerItemUpdate!!.dateCreated)
        }
        if (action == "get") {
            setViewOnAction(resources.getColor(R.color.color_3_dark))
            btnCalculator.setBackgroundDrawable(resources.getDrawable(R.drawable.view_all_round_corners))
            btnSave.setBackgroundDrawable(resources.getDrawable(R.drawable.view_all_round_corners))
        } else {
            setViewOnAction(resources.getColor(R.color.red))
            btnCalculator.setBackgroundDrawable(resources.getDrawable(R.drawable.view_all_round_corners_red))
            btnSave.setBackgroundDrawable(resources.getDrawable(R.drawable.view_all_round_corners))
        }
    }

    private fun setViewOnAction(color: Int) {
        tvRupess.setTextColor(color)
        etAmount.setTextColor(color)
        tvDate.setTextColor(color)
        // header.setBackgroundColor(color)

    }

    private fun clickListeners() {

        etAmount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                checkEditedValue(s)
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkEditedValue(s)
            }

            override fun afterTextChanged(s: Editable) {
                checkEditedValue(s)
            }

        })


        etAmount.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }


        llDate.setOnClickListener {
            view = it
            pick_date()
        }

        btnCalculator.setOnClickListener {
            view = it
            CalculatorBuilder()
                .withTitle("Calculate Amount")
                .withValue(Amount)
                .start(this@CustomerItemAddUpdateRemove)
        }

        btnSave.setOnClickListener {
            if (Util.DETECT_INTERNET_CONNECTION(context)) {
                date = tvDate.text.toString()
                itemDetail = etItemDetail.text.toString()
                if (action == "get") {
                    amountGet = etAmount.text.toString().replace(",", "")
                } else {
                    amountGive = etAmount.text.toString().replace(",", "")
                }

                if (customerItem != null) {
                    customerId = customerItem?.id!!
                    businessId = customerItem?.businessId!!
                } else {
                    customerId = customerItemUpdate?.cusomerId!!
                    businessId = customerItemUpdate?.businessId!!
                    recordId = customerItemUpdate?.id!!

                }

                if (etAmount.text!!.isNotEmpty()) {
                    if (function == "add") {
                        pDialog!!.show()
                        AddItemToServer(
                            customerId,
                            businessId,
                            date,
                            amountGet,
                            amountGive,
                            itemDetail
                        )
                    } else {
                        pDialog!!.show()
                        UpdateItemToServer(
                            customerId,
                            businessId,
                            date,
                            amountGet,
                            amountGive,
                            itemDetail,
                            recordId
                        )
                    }
                } else {
                    Util.showDialog("Alert", "Please enter Amount", context)
                }
            } else {
                Util.showDialog("Alert", "No Internet Connection", context)
            }
        }

        tvRemove.setOnClickListener {

            showDialog("Alert", "Do you want to delete item.", context)

        }

        ImBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun deletWebServiceCalling() {

        customerId = customerItemUpdate?.cusomerId!!
        businessId = customerItemUpdate?.businessId!!
        recordId = customerItemUpdate?.id!!
        if (Util.DETECT_INTERNET_CONNECTION(context)) {
            if (etAmount.text!!.isNotEmpty()) {
                pDialog!!.show()
                DeleteItemToServer(
                    customerId,
                    businessId,
                    recordId
                )
            } else {
                Util.showDialog("Alert", "Please enter Amount", context)
            }
        } else {
            Util.showDialog("Alert", "No Internet Connection", context)
        }
    }

    private fun checkEditedValue(s: CharSequence) {
        if (s.isEmpty()) {
            btnSave.alpha = 0.4f
        } else {
            btnSave.alpha = 1f
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode === CalculatorActivity.REQUEST_RESULT_SUCCESSFUL) {
            val result = data!!.getStringExtra(CalculatorActivity.RESULT)
            Amount = result
            etAmount.setText(Amount)
        }
    }


    private fun AddItemToServer(
        customerId: String,
        businessId: String,
        date: String,
        amountGet: String,
        amountGive: String,
        itemDetail: String
    ) {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)

        val requestCall =
            destinationService.addItemDetailFromServer(
                DataBinding.setCustomerItemDetail(
                    userInfo,
                    customerId,
                    businessId,
                    date,
                    amountGet,
                    amountGive,
                    itemDetail
                )
            )
        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        finish()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }

    private fun UpdateItemToServer(
        customerId: String,
        businessId: String,
        date: String,
        amountGet: String,
        amountGive: String,
        itemDetail: String,
        recordId: String
    ) {

        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)

        val requestCall =
            destinationService.updateItemDetailFromServer(
                DataBinding.setUpdateCustomerItemDetail(
                    userInfo,
                    customerId,
                    businessId,
                    date,
                    amountGet,
                    amountGive,
                    itemDetail,
                    recordId
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        finish()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )

    }


    private fun DeleteItemToServer(customerId: String, businessId: String, recordId: String) {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)

        val requestCall =
            destinationService.deleteItemDetailFromServer(
                DataBinding.deleteCustomerItemDetail(
                    userInfo,
                    customerId,
                    businessId,
                    recordId
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        finish()
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )
    }


    private fun pick_date() {
        val now = Calendar.getInstance()

        val dialog = DatePickerFragmentDialog.newInstance({ view, year, monthOfYear, dayOfMonth ->
            var _Date = -1
            var _Month = -1
            var _Year = -1

            _Date = dayOfMonth
            _Month = monthOfYear + 1
            _Year = year

            var Date_Set = Util.getFormattedDate_set_task("$_Year/$_Month/$_Date")
            tvDate.text = Date_Set

        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH))

        dialog.show(supportFragmentManager, "tag")
    }

    fun showDialog(title: String, message: String, contaxt: Context) {


        AndExAlertDialog.Builder(contaxt)
            .setTitle(title)
            .setMessage(message)
            .setPositiveBtnText("ok")
            .setNegativeBtnText("cancel")
            .setCancelableOnTouchOutside(false)
            .OnPositiveClicked {
                deletWebServiceCalling()
            }
            .OnNegativeClicked {

            }
            .build()

    }

}

