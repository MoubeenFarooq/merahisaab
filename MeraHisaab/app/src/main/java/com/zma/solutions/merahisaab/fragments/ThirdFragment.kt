package com.zma.solutions.merahisaab.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.zma.solutions.merahisaab.R

class ThirdFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.screen_fragment, container, false)

        val llthird = view.findViewById<LinearLayout>(R.id.llThird)
        llthird.visibility = View.VISIBLE

        return view
    }

}
