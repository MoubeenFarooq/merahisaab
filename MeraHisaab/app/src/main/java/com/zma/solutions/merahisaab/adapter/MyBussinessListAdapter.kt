package com.zma.solutions.merahisaab.adapter

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.listeners.BussinessItemClickListener
import com.zma.solutions.merahisaab.model.BusinessList
import kotlinx.android.synthetic.main.adapter_business_item.view.*


class MyBussinessListAdapter(
    val context: Context,
    val businessList: List<BusinessList.BusinessItem>,
    val businessItem: (BussinessItemClickListener)
) :
    RecyclerView.Adapter<MyBussinessListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.adapter_business_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return businessList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val hobbyItem = businessList[position]
        holder.setDate(hobbyItem, position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var currentItem: BusinessList.BusinessItem? = null

        init {
            itemView.llBusinessItemClick.setOnClickListener {
                businessItem!!.businessItem(currentItem!!)
            }

            itemView.imOption.setOnClickListener {
                showMenuOption(it, currentItem)
            }
        }

        fun setDate(
            businessItem: BusinessList.BusinessItem?,
            pos: Int
        ) {//? and !! use for not null
            Util.FONT_AWSOME(context, itemView.imOption)
            itemView.tvBusinessNameItem.text = businessItem!!.businessName
            itemView.tvCustomerNumber.text = businessItem.TotalCustomers
            itemView.tvDateCreated.text = businessItem.date
            this.currentItem = businessItem
        }
    }

    private fun showMenuOption(
        it: View,
        currentItem: BusinessList.BusinessItem?
    ) {
        val popup = PopupMenu(context, it)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.menu_business, popup.menu)
        popup.setOnMenuItemClickListener {
            return@setOnMenuItemClickListener when (it.itemId) {
                R.id.action_update -> {
                    businessItem!!.businessItemUpdate(currentItem!!)
                    true
                }
                R.id.action_delete -> {
                    if (Util.DETECT_INTERNET_CONNECTION(context)) {
                        businessItem!!.businessItemDelete(currentItem!!)
                    } else {
                        Util.showDialog("Alert", "No Internet Connection", context)
                    }
                    true
                }
                else -> false
            }
        }
        popup.gravity = Gravity.END
        popup.show()
    }
}



