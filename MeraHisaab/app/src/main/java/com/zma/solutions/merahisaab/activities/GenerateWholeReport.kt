package com.zma.solutions.merahisaab.activities

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.itextpdf.text.*
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.pdf.draw.VerticalPositionMark
import com.zma.solutions.merahisaab.R
import com.zma.solutions.merahisaab.Utils.DataBinding
import com.zma.solutions.merahisaab.Utils.FileUtils
import com.zma.solutions.merahisaab.Utils.PrefManger
import com.zma.solutions.merahisaab.Utils.Util
import com.zma.solutions.merahisaab.adapter.MyReportListWholeAdapter
import com.zma.solutions.merahisaab.model.ReportData
import com.zma.solutions.merahisaab.model.UserInfo
import com.zma.solutions.merahisaab.model.WebservicesCalling
import com.zma.solutions.merahisaab.service.ServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ir.androidexception.andexalertdialog.AndExAlertDialog
import kotlinx.android.synthetic.main.activity_report_view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlinx.android.synthetic.main.activity_adding_update_item.*
import kotlinx.android.synthetic.main.activity_report_view.*
import kotlinx.android.synthetic.main.activity_report_view.ImBack
import java.net.URLConnection


class GenerateWholeReport : AppCompatActivity() {

    var context: GenerateWholeReport = this
    var reportList: ReportData? = null
    var userInfo: UserInfo? = null
    var mReportList: List<ReportData.ReportItemDetail>? = null
    var businessName = ""
    var userNumber = ""
    var totalGot = ""
    var totalGave = ""
    var totalAmount = ""
    var totalNet = ""
    var pDialog: SweetAlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_report_view)

        pDialog = Util.init_Progress(context)

        clickListeners()

        getDataFromPreviousActivity()

        Util.FONT_AWSOME(context, reportBtn)
        Util.FONT_AWSOME(context, ImBack)

    }


    private fun clickListeners() {

        val layoutManger = LinearLayoutManager(this)
        layoutManger.orientation = LinearLayoutManager.VERTICAL
        listItems.layoutManager = layoutManger

        reportBtn.setOnClickListener {
            pDialog!!.show()
            setDataForReport()
        }
        ImBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setDataForReport() {

        userNumber = userInfo!!.userPhone.toString()

        businessName = userInfo!!.businessName.toString()

        settingGetAndGot(reportList)


    }

    private fun settingGetAndGot(reportList: ReportData?) {
        mReportList = reportList!!.reportItemDetail
        var totalGetAmount = 0
        var totalGiveAmount = 0

        for (i in mReportList!!.indices) {
            totalGetAmount += mReportList!![i].youGot!!.toInt()
        }

        for (i in mReportList!!.indices) {
            totalGiveAmount += mReportList!![i].youGave!!.toInt()
        }

        totalGot = totalGetAmount.toString()
        totalGave = totalGiveAmount.toString()

        totalNet = if (totalGave.toInt() > totalGot.toInt()) {
            ((totalGave.toInt() - totalGot.toInt()).toString())
        } else {
            ((totalGot.toInt() - totalGave.toInt()).toString())
        }

        createPdf(FileUtils.getAppPath(context) + Util.pick_date() + ".pdf")



    }


    private fun getDataFromPreviousActivity() {
        userInfo = PrefManger.getUser(context)

        userNumber = userInfo!!.userPhone.toString()

        businessName = userInfo!!.businessName.toString()


        if (Util.DETECT_INTERNET_CONNECTION(context)) {
            pDialog!!.show()
            getWholeBusinessDataFromServer(userInfo!!)
        } else {
            Util.showDialog("Alert", "No Internet Connection", context)
        }
    }

    private fun getWholeBusinessDataFromServer(userInfo: UserInfo) {
        val destinationService = ServiceBuilder.buildService(WebservicesCalling::class.java)
        val requestCall =
            destinationService.getAllBusinessCustomerFromsServer(
                DataBinding.getAllBusinessCustomer(
                    userInfo
                )
            )

        requestCall.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    pDialog!!.hide()
                    if (it.status.equals("true")) {
                        reportList = it
                        settingMainGetAndGot(it.reportItemDetail)
                    } else {
                        Log.e("TAG", it.message.toString())
                    }
                },
                {
                    pDialog!!.hide()
                    Log.e("TAG", it.cause.toString())
                }
            )

    }


    private fun settingMainGetAndGot(reportList: List<ReportData.ReportItemDetail>) {
        var totalGetAmount = 0
        var totalGiveAmount = 0

        for (i in reportList!!.indices) {
            totalGetAmount += reportList[i].youGot!!.toInt()
        }

        for (i in reportList.indices) {
            totalGiveAmount += reportList[i].youGave!!.toInt()
        }

        totalGot = totalGetAmount.toString()
        totalGave = totalGiveAmount.toString()

        totalAmount = if (totalGave.toInt() > totalGot.toInt()) {
            ((totalGave.toInt() - totalGot.toInt()).toString())
        } else {
            ((totalGot.toInt() - totalGave.toInt()).toString())
        }

        tvMainGet.text = totalGot
        tvMainGot.text = totalGave

        tvNetBalance.text = totalAmount

        val adapter = MyReportListWholeAdapter(context, reportList)
        listItems.adapter = adapter

    }


    fun createPdf(dest: String){

        if (File(dest).exists()) {
            File(dest).delete()
        }

        try {
            /**
             * Creating Document
             */
            var document = Document()

            // Location to save
            PdfWriter.getInstance(document, FileOutputStream(dest))

            // Open to write
            document.open()

            // Document Settings
            document.pageSize = PageSize.A4
            document.addCreationDate()
            document.addAuthor("Mera Hisaab")
            document.addCreator("ZMA")


            /**
             * How to USE FONT....
             */
            var urName =
                BaseFont.createFont(
                    "assets/font/san_francisco_display_regular.otf",
                    "UTF-8",
                    BaseFont.EMBEDDED
                )

            var mFonts =
                BaseFont.createFont(
                    "assets/font/helvetica_neue_light.ttf",
                    "UTF-8",
                    BaseFont.NOT_EMBEDDED
                )

            // LINE SEPARATOR
            var lineSeparator = LineSeparator()
            lineSeparator.lineColor = BaseColor(0, 0, 0, 68)


            var fontGreen = BaseColor(0, 105, 92, 255)
            var fontRed = BaseColor(165, 36, 36, 255)
            var fontGrey = BaseColor(91, 86, 82, 255)

            var mTitleFont = Font(urName, 15.0f, Font.NORMAL, BaseColor.BLACK)
            var mGotFont = Font(mFonts, 15.0f, Font.NORMAL, fontGreen)
            var mGiveFont = Font(mFonts, 15.0f, Font.NORMAL, fontRed)
            var mTotalFont = Font(mFonts, 15.0f, Font.NORMAL, fontGrey)

            var myImg = setHeaderlogo()

            addHeaderLeftToRightImage(document, myImg, "Business Name: $businessName", mTitleFont)

            addHeaderLeftToRight(
                document,
                "",
                "Business Number: $userNumber",
                mTitleFont
            )


            lineSeparators(document, lineSeparator)


            // Adding Title....
            var mOrderDetailsTitleFont = Font(urName, 25.0f, Font.NORMAL, BaseColor.BLACK)
            var mOrderDetailsTitleChunk = Chunk("Transactions Report", mOrderDetailsTitleFont)
            var mOrderDetailsTitleParagraph = Paragraph(mOrderDetailsTitleChunk)
            mOrderDetailsTitleParagraph.alignment = Element.ALIGN_CENTER
            document.add(mOrderDetailsTitleParagraph)


            addLeftText(
                document,
                "No of Transaction: ", "" + mReportList!!.size,
                mTitleFont
            )

            addLeftText(
                document,
                "Total Gave: ", "Rs: $totalGave",
                mGiveFont
            )
            addLeftText(
                document,
                "Total Got: ", "Rs: $totalGot",
                mGotFont
            )


            addLeftText(
                document,
                "Total Amount: ", "Rs: $totalNet",
                mTotalFont
            )

            document.add(Paragraph(""))

            document.add(Chunk.NEWLINE)

            var floatArray = floatArrayOf(10f, 40f, 30f, 20f, 20f)
            var table = PdfPTable(5)
            table.setWidths(floatArray)
            table.widthPercentage = 100f
            table.spacingBefore = 10f
            table.totalWidth = 288f
            var mHeaderFont = Font(urName, 16.0f, Font.NORMAL, BaseColor.BLACK)

            createCellInTable(table, "Sr", mHeaderFont)
            createCellInTable(table, "Date", mHeaderFont)
            createCellInTable(table, "Detail", mHeaderFont)
            createCellInTable(table, "You Gave", mHeaderFont)
            createCellInTable(table, "You Got", mHeaderFont)

            table.headerRows = 1

            settingDataField(document, table, lineSeparator, mTitleFont, mGiveFont, mGotFont)

            document.add(Chunk.NEWLINE)

            document.add(Paragraph("Document Generated On - " + Util.pick_date()))

            document.close()

            pDialog!!.hide()

            showDialog(context, "Choose", "Please choose one option", dest)

        } catch (ie: IOException) {

        } catch (ae: ActivityNotFoundException) {

        }
    }

    private fun showDialog(context: Context, title: String, message: String, dest: String) {
        AndExAlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveBtnText("Share")
            .setNegativeBtnText("Adobe Reader")
            .setCancelableOnTouchOutside(false)
            .OnPositiveClicked {
                if (Util.DETECT_INTERNET_CONNECTION(context)) {

                    val intentShareFile = Intent(Intent.ACTION_SEND)

                    val apkURI: Uri = FileProvider.getUriForFile(
                        applicationContext,
                        applicationContext
                            .packageName + ".provider", File(dest)!!
                    )
                    intentShareFile.setDataAndType(
                        apkURI,
                        URLConnection.guessContentTypeFromName(File(dest).name)
                    )
                    intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                    intentShareFile.putExtra(
                        Intent.EXTRA_STREAM,
                        Uri.parse("file://" + File(dest).absolutePath)
                    )

                    startActivity(Intent.createChooser(intentShareFile, "Share File"))

                } else {
                    Util.showDialog("Alert", "No Internet Connection", context)
                }
            }.OnNegativeClicked {
                if (Util.DETECT_INTERNET_CONNECTION(context)) {
                    FileUtils.openFile(context, File(dest))
                } else {
                    Util.showDialog("Alert", "No Internet Connection", context)
                }
            }
            .build()

    }


    private fun createCellInTable(
        table: PdfPTable,
        text: String,
        mHeaderFont: Font
    ) {
        // Adding cells to the table
        var cell = PdfPCell()
        var p = Paragraph()
        var chunkLeft = Chunk(text, mHeaderFont)
        p.alignment = Element.ALIGN_MIDDLE
        p.add(chunkLeft)
        cell.addElement(p)
        cell.border = Rectangle.NO_BORDER
        cell.backgroundColor = BaseColor.LIGHT_GRAY
        table.addCell(cell)

    }

    private fun settingDataField(
        document: Document,
        table: PdfPTable,
        lineSeparator: LineSeparator,
        mTitleFont: Font,
        mGiveFont: Font,
        mGotFont: Font
    ) {

        for (i in mReportList!!.indices) {

            table.addCell(setCellValue("" + (i + 1), mTitleFont))
            var youGave = mReportList!![i].youGave
            var youGot = mReportList!![i].youGot

            if (youGave == "0") youGave = "-"
            if (youGot == "0") youGot = "-"

            table.addCell(
                setCellValue(
                    "" + mReportList!![i].dateCreated,
                    mTitleFont
                )
            )

            table.addCell(
                setCellValue(
                    "" + mReportList!![i].details,
                    mTitleFont
                )
            )

            table.addCell(
                setCellValue(
                    "" + youGave,
                    mGiveFont
                )
            )

            table.addCell(
                setCellValue(
                    "" + youGot,
                    mGotFont
                )
            )

            document.add(Paragraph(""))
        }

        document.add(table)
    }

    private fun setCellValue(value: String, titleFont: Font): PdfPCell {
        var cell = PdfPCell()


        var chunkL = Chunk(value, titleFont)
        cell.addElement(chunkL)
        cell.setPadding(5f)
        cell.borderColorLeft = BaseColor.WHITE
        cell.borderColorRight = BaseColor.WHITE
        cell.borderColorBottom = BaseColor.LIGHT_GRAY
        cell.borderColorTop = BaseColor.WHITE
        return cell

    }


    private fun addLeftText(
        document: Document,
        mHeader: String,
        mValue: String,
        mFont: Font
    ) {

        var floatArray = floatArrayOf(10f, 10f)

        var p = Paragraph()
        var table = PdfPTable(2)
        table.horizontalAlignment = Element.ALIGN_LEFT
        //  table.totalWidth = 200f
        table.setWidths(floatArray)

        var cell = PdfPCell()
        var chunkL = Chunk(mHeader, mFont)
        cell.addElement(chunkL)
        cell.border = Rectangle.NO_BORDER
        table.addCell(cell)

        var cell1 = PdfPCell()
        var chunkR = Chunk(mValue, mFont)
        cell1.addElement(chunkR)
        cell1.border = Rectangle.NO_BORDER
        table.addCell(cell1)

        p.indentationLeft = 5f
        p.add(table)
        document.add(p)

    }

    private fun lineSeparators(
        document: Document,
        lineSeparator: LineSeparator
    ) {
        // Adding Line Breakable Space....
        document.add(Paragraph(""))
        // Adding Horizontal Line...
        document.add(Chunk(lineSeparator))
        // Adding Line Breakable Space....
        document.add(Paragraph(""))

    }

    private fun addHeaderLeftToRight(
        document: Document,
        title: String,
        number: String,
        mTitleFont: Font
    ) {
        var chunkLeft = Chunk(title, mTitleFont)
        var chunkRight = Chunk(number, mTitleFont)

        var mOrderDetailsTitleParagraph = Paragraph(chunkLeft)
        mOrderDetailsTitleParagraph.add(Chunk(VerticalPositionMark()))
        mOrderDetailsTitleParagraph.add(chunkRight)
        document.add(mOrderDetailsTitleParagraph)

    }


    private fun addHeaderLeftToRightImage(
        document: Document,
        myImg: Image?,
        businessTitle: String,
        mTitleFont: Font
    ) {
        var chunkRight = Chunk(businessTitle, mTitleFont)
        var chunkLeft = Chunk(myImg, 0F, 0F, true)
        var mOrderDetailsTitleParagraph = Paragraph(chunkLeft)
        mOrderDetailsTitleParagraph.add(Chunk(VerticalPositionMark()))
        mOrderDetailsTitleParagraph.add(chunkRight)
        document.add(mOrderDetailsTitleParagraph)
    }


    private fun setHeaderlogo(): Image? {

        var bm = BitmapFactory.decodeResource(resources, R.mipmap.logo_image)
        var stream = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream)
        var myImg = Image.getInstance(stream.toByteArray())
        myImg.width
        myImg.alignment = Image.ALIGN_LEFT
        myImg.border = Rectangle.NO_BORDER
        return myImg
    }
}
