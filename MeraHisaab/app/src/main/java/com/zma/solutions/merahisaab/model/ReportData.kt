package com.zma.solutions.merahisaab.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReportData(

    @SerializedName("status")
    @Expose
    var status: String?,

    @SerializedName("message")
    @Expose
    var message: String?,

    @SerializedName("customerDetailList")
    @Expose
    var reportItemDetail: List<ReportItemDetail>
) : Parcelable {
    class ReportItemDetail(

        @SerializedName("id")
        @Expose
        var id: String?,

        @SerializedName("DateCreated")
        @Expose
        var dateCreated: String?,

        @SerializedName("CustomerId")
        @Expose
        var customerId: String?,

        @SerializedName("Details")
        @Expose
        var details: String?,

        @SerializedName("YouGave")
        @Expose
        var youGave: String?,

        @SerializedName("UserId")
        @Expose
        var userId: String?,

        @SerializedName("YouGot")
        @Expose
        var youGot: String?,

        @SerializedName("BusinessId")
        @Expose
        var businessId: String?,

        @SerializedName("CusomerId")
        @Expose
        var cusomerId: String?
    ) : Parcelable {
        constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(dateCreated)
            writeString(customerId)
            writeString(details)
            writeString(youGave)
            writeString(userId)
            writeString(youGot)
            writeString(businessId)
            writeString(cusomerId)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<ReportItemDetail> =
                object : Parcelable.Creator<ReportItemDetail> {
                    override fun createFromParcel(source: Parcel): ReportItemDetail =
                        ReportItemDetail(source)

                    override fun newArray(size: Int): Array<ReportItemDetail?> = arrayOfNulls(size)
                }
        }
    }

    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        ArrayList<ReportItemDetail>().apply {
            source.readList(
                this as List<*>,
                ReportItemDetail::class.java.classLoader
            )
        }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(status)
        writeString(message)
        writeList(reportItemDetail)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ReportData> = object : Parcelable.Creator<ReportData> {
            override fun createFromParcel(source: Parcel): ReportData = ReportData(source)
            override fun newArray(size: Int): Array<ReportData?> = arrayOfNulls(size)
        }
    }
}