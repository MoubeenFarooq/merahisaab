package com.zma.solutions.merahisaab.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CustomerList(
    @SerializedName("status")
    @Expose
    var status: String?,
    @SerializedName("message")
    @Expose
    var message: String?,
    @SerializedName("customerList")
    @Expose
    var customerList: MutableList<CustomerListItem>
) : Parcelable {

    class CustomerListItem(

        @SerializedName("id")
        @Expose
        var id: String? = null,

        @SerializedName("YouWillGive")
        @Expose
        var youWillGive: String? = null,

        @SerializedName("YouWillGet")
        @Expose
        var youWillGet: String? = null,

        @SerializedName("BusinessId")
        @Expose
        var businessId: String? = null,

        @SerializedName("UserId")
        @Expose
        var userId: String? = null,

        @SerializedName("CustomerName")
        @Expose
        var customerName: String? = null,

        @SerializedName("CustomerPhoneNumber")
        @Expose
        var customerPhoneNumber: String? = null,

        @SerializedName("DateCreated")
        @Expose
        var createDateAndTime: String? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(youWillGive)
            writeString(youWillGet)
            writeString(businessId)
            writeString(userId)
            writeString(customerName)
            writeString(customerPhoneNumber)
            writeString(createDateAndTime)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<CustomerListItem> =
                object : Parcelable.Creator<CustomerListItem> {
                    override fun createFromParcel(source: Parcel): CustomerListItem =
                        CustomerListItem(source)

                    override fun newArray(size: Int): Array<CustomerListItem?> = arrayOfNulls(size)
                }
        }
    }

    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        ArrayList<CustomerListItem>().apply {
            source.readList(
                this as List<*>,
                CustomerListItem::class.java.classLoader
            )
        }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(status)
        writeString(message)
        writeList(customerList as List<*>?)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CustomerList> = object : Parcelable.Creator<CustomerList> {
            override fun createFromParcel(source: Parcel): CustomerList = CustomerList(source)
            override fun newArray(size: Int): Array<CustomerList?> = arrayOfNulls(size)
        }
    }
}