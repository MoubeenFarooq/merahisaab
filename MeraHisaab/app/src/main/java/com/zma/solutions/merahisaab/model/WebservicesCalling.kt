package com.zma.solutions.merahisaab.model

import com.google.gson.JsonObject
import com.zma.solutions.merahisaab.Utils.Constant
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface WebservicesCalling {

    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.USER + "/" + Constant.ADDUSER_ENDPOINT))
    fun setUserDataOnServer(@Body userInfo: JsonObject): Observable<UserInfo>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.BUSINESS + "/" + Constant.ADDBUSINESS_ENDPOINT))
    fun addBusinessToServer(@Body userInfo: JsonObject): Observable<UserInfo>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.BUSINESS + "/" + Constant.GETALLBUSINESS_ENDPOINT))
    fun getAllBusinessFromServer(@Body userInfo: JsonObject): Observable<BusinessList>

    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER + "/" + Constant.GETALLCUSTOMER_ENDPOINT))
    fun getAllCustomerFromServer(@Body userInfo: JsonObject): Observable<CustomerList>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER + "/" + Constant.ADD_CUSTOMER))
    fun addCustomerToServer(@Body addCustomer: JsonObject): Observable<CustomerList>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER_DETAIL + "/" + Constant.GET_CUSTOMER_DETAIL))
    fun getCustomerItemDetailFromServer(@Body addCustomer: JsonObject): Observable<CustomerItemDetail>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER_DETAIL + "/" + Constant.ADD_CUSTOMER_DETAIL))
    fun addItemDetailFromServer(@Body addCustomer: JsonObject): Observable<CustomerItemDetail>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER_DETAIL + "/" + Constant.UPDATE_CUSTOMER_DETAIL))
    fun updateItemDetailFromServer(@Body updateCustomerItemDetail: JsonObject): Observable<CustomerItemDetail>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER_DETAIL + "/" + Constant.DELETE_CUSTOMER_DETAIL))
    fun deleteItemDetailFromServer(@Body deleteCustomerItemDetail: JsonObject): Observable<CustomerItemDetail>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER + "/" + Constant.DELETE_CUSTOMER))
    fun deleteCustomerFromServer(@Body customerDetail: JsonObject): Observable<CustomerItemDetail>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.CUSTOMER + "/" + Constant.UPDATE_CUSTOMER))
    fun updateCustomerToServer(@Body updateCustomer: JsonObject): Observable<NormalResponse>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.BUSINESS + "/" + Constant.UPDATE_BUSINESS))
    fun updateBusinessToServer(@Body updateBusiness: JsonObject): Observable<NormalResponse>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.BUSINESS + "/" + Constant.DELETE_BUSINESS))
    fun deleteBusinessToServer(@Body deleteBusiness: JsonObject): Observable<NormalResponse>


    //samaple call RX java
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @POST((Constant.REPORT + "/" + Constant.GETREPORTDATA))
    fun getAllBusinessCustomerFromsServer(@Body allBusinessCustomer: JsonObject): Observable<ReportData>


}