package com.zma.solutions.merahisaab.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class CustomerItemDetail(
    @SerializedName("status")
    @Expose
    var status: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("YouWillGive")
    @Expose
    var youWillGive: String? = null,

    @SerializedName("YouWillGet")
    @Expose
    var youWillGet: String? = null,

    @SerializedName("customerDetailList")
    @Expose
    var customerDetailList: List<CustomerDetailListItem>? = null
) : Parcelable {
    class CustomerDetailListItem(
        @SerializedName("id")
        @Expose
        var id: String? = null,

        @SerializedName("UserId")
        @Expose
        var userId: String? = null,

        @SerializedName("BusinessId")
        @Expose
        var businessId: String? = null,

        @SerializedName("YouGot")
        @Expose
        var youGot: String? = null,

        @SerializedName("CustomerId")
        @Expose
        var cusomerId: String? = null,

        @SerializedName("DateCreated")
        @Expose
        var dateCreated: String? = null,

        @SerializedName("Details")
        @Expose
        var details: String? = null,

        @SerializedName("YouGave")
        @Expose
        var youGave: String? = null
    ) : Parcelable {
        constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString()
        )

        override fun describeContents() = 0

        override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
            writeString(id)
            writeString(userId)
            writeString(businessId)
            writeString(youGot)
            writeString(cusomerId)
            writeString(dateCreated)
            writeString(details)
            writeString(youGave)
        }

        companion object {
            @JvmField
            val CREATOR: Parcelable.Creator<CustomerDetailListItem> =
                object : Parcelable.Creator<CustomerDetailListItem> {
                    override fun createFromParcel(source: Parcel): CustomerDetailListItem =
                        CustomerDetailListItem(source)

                    override fun newArray(size: Int): Array<CustomerDetailListItem?> =
                        arrayOfNulls(size)
                }
        }
    }

    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        ArrayList<CustomerDetailListItem>().apply {
            source.readList(
                this as List<*>,
                CustomerDetailListItem::class.java.classLoader
            )
        }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(status)
        writeString(message)
        writeString(youWillGive)
        writeString(youWillGet)
        writeList(customerDetailList)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CustomerItemDetail> =
            object : Parcelable.Creator<CustomerItemDetail> {
                override fun createFromParcel(source: Parcel): CustomerItemDetail =
                    CustomerItemDetail(source)

                override fun newArray(size: Int): Array<CustomerItemDetail?> = arrayOfNulls(size)
            }
    }
}