package com.zma.solutions.merahisaab.listeners

import com.zma.solutions.merahisaab.model.CustomerList

interface CustomerItemClickListener {
    fun customerItem(customerItem: CustomerList.CustomerListItem)
}