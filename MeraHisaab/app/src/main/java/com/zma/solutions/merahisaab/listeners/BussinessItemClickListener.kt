package com.zma.solutions.merahisaab.listeners

import com.zma.solutions.merahisaab.model.BusinessList

interface BussinessItemClickListener {
    fun businessItem(businessItem: BusinessList.BusinessItem)
    fun businessItemUpdate(businessItem: BusinessList.BusinessItem)
    fun businessItemDelete(businessItem: BusinessList.BusinessItem)
}