package com.zma.solutions.merahisaab.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NormalResponse {

    @SerializedName("status")
    @Expose
    var status: Boolean = false

    @SerializedName("message")
    @Expose
    var message: String? = null
}