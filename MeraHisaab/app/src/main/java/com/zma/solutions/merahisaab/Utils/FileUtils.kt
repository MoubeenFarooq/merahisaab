package com.zma.solutions.merahisaab.Utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.content.FileProvider
import com.zma.solutions.merahisaab.BuildConfig
import java.io.File
import java.io.IOException


object FileUtils {

    private val extensions = arrayOf(
        "pdf",
        "docx",
        "doc",
        "txt"
    )


    @Throws(ActivityNotFoundException::class, IOException::class)
    fun openFile(context: Context, url: File) {
        // Create URI
        //Uri uri = Uri.fromFile(url);

        //TODO you want to use this method then create file provider in androidmanifest.xml with fileprovider name

        val uri = FileProvider.getUriForFile(
            context,
            BuildConfig.APPLICATION_ID + ".provider",
            url
        )

        val urlString = url.toString().toLowerCase()

        val intent = Intent(Intent.ACTION_VIEW)

        /**
         * Security
         */
        val resInfoList = context.packageManager
            .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            context.grantUriPermission(
                packageName,
                uri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }

        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (urlString.toLowerCase().toLowerCase().contains(".doc") || urlString.toLowerCase().contains(
                ".docx"
            )
        ) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (urlString.toLowerCase().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (urlString.toLowerCase().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else {
            // if you want you can also define the intent type for any other file

            // additionally use else clause below, to manage other unknown extensions
            // in this case, Android will show all applications installed on the device
            // so you can choose which application to use
            intent.setDataAndType(uri, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

    /**
     * Get Path of App which contains Files
     *
     * @return path of root dir
     */
    fun getAppPath(context: Context): String {
        var rootPath: String
        rootPath = context.getExternalFilesDir(null)!!.absolutePath
        // extraPortion is extra part of file path
        val extraPortion = ("Android/data/" + BuildConfig.APPLICATION_ID
                + File.separator + "files" + File.separator)
        // Remove extraPortion
        rootPath = rootPath.replace(extraPortion, "")

        val dir = File(rootPath)
        if (!dir.exists()) {
            dir.mkdir()
        }
        return dir.path + File.separator
    }

    /*  */
    /***
     * Copy File
     *
     * @param src
     * @param dst
     * @throws IOException
     *//*
    fun copy(src: File, dst: File) {
        val `in`: InputStream
        val out: OutputStream?
        try {
            `in` = FileInputStream(src)
            out = FileOutputStream(dst)

            val tempExt = getExtension(dst.getPath())


            if ((tempExt == "jpeg" || tempExt == "jpg" || tempExt == "gif"
                        || tempExt == "png")
            ) {
                if (out != null) {

                    var bit = BitmapFactory.decodeFile(src.getPath())

                    if (bit.width > 700) {
                        if (bit.height > 700)
                            bit = Bitmap.createScaledBitmap(bit, 700, 700, true)
                        else
                            bit = Bitmap.createScaledBitmap(bit, 700, bit.height, true)
                    } else {
                        if (bit.height > 700)
                            bit = Bitmap.createScaledBitmap(bit, bit.width, 700, true)
                        else
                            bit = Bitmap.createScaledBitmap(bit, bit.width, bit.height, true)
                    }

                    bit.compress(Bitmap.CompressFormat.JPEG, 90, out)
                }

            } else {

                // Transfer bytes from in to out
                val buf = ByteArray(1024 * 4)
                val len: Int
                while ((len = `in`.read(buf)) > 0) {
                    out!!.write(buf, 0, len)
                }
            }

            `in`.close()
            out!!.close()

        } catch (e: FileNotFoundException) {
            // TODO Auto-generated catch block

        } catch (e: IOException) {
            // TODO Auto-generated catch block

        } catch (e: Exception) {
            // TODO: handle exception

        }

    }

    */
    /***
     * Move File
     *
     * @param src
     * @param dst
     * @throws IOException
     *//*
    fun move(src: File, dst: File) {
        val `in`: InputStream
        val out: OutputStream?
        try {
            `in` = FileInputStream(src)
            out = FileOutputStream(dst)

            val tempExt = getExtension(dst.getPath())

            if ((tempExt == "jpeg" || tempExt == "jpg" || tempExt == "gif"
                        || tempExt == "png")
            ) {
                if (out != null) {

                    var bit = BitmapFactory.decodeFile(src.getPath())


                    if (bit.width > 700 || bit.height > 700) {
                        bit = Bitmap.createScaledBitmap(bit, 700, 700, true)
                    }
                    bit.compress(Bitmap.CompressFormat.JPEG, 90, out)
                }

            } else {

                // Transfer bytes from in to out
                val buf = ByteArray(1024 * 4)
                val len: Int
                while ((len = `in`.read(buf)) > 0) {
                    out!!.write(buf, 0, len)
                }
            }

            `in`.close()
            out!!.close()


        } catch (e: FileNotFoundException) {
            // TODO Auto-generated catch block

        } catch (e: IOException) {
            // TODO Auto-generated catch block

        } catch (e: Exception) {
            // TODO: handle exception

        }

    }

    */
    /**
     * Is Valid Extension
     *
     * @param ext
     * @return
     *//*
    fun isValidExtension(ext: String): Boolean {
        return Arrays.asList(extensions).contains(ext)

    }*/

    /**
     * Return Extension of given path without dot(.)
     *
     * @param path
     * @return
     */
    fun getExtension(path: String): String {
        return if (path.contains(".")) path.substring(path.lastIndexOf(".") + 1).toLowerCase() else ""
    }
}