package com.zma.solutions.merahisaab.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddBusinessDetail {
    @SerializedName("BusinessName")
    @Expose
    var businessName: String? = null

    @SerializedName("BusinessId")
    @Expose
    var businessId: String? = null

    @SerializedName("BusinessUpdateTime")
    @Expose
    var businessUpdateTime: String? = null

    @SerializedName("DateCreated")
    @Expose
    var createDateAndTime: String? = null

    @SerializedName("MessageType")
    @Expose
    var messageType: String? = null

    @SerializedName("status")
    @Expose
    var status: Boolean = false

    @SerializedName("message")
    @Expose
    var message: String? = null

}
